#!/opt/fmrib/conda/python3/envs/system-tools/bin/python

import unittest
import builtins
import tempfile
import errno
import os
import grp
import stat
import time
import shutil
from pathlib import Path
from unittest.mock import patch, call, Mock
import random, string
import move_fsl
import logging
import subprocess as sp
import pdb
import warnings
import io
from contextlib import redirect_stderr
try:
    import magic
    NO_MAGIC=False
except ImportError:
    NO_MAGIC=True
    
class TestGlobals(unittest.TestCase):
    def test_add_skip_ext(self):
        self.assertRaises(TypeError, move_fsl.add_skip_ext, 0.0)
        self.assertRaises(TypeError, move_fsl.add_skip_ext, 0)
        move_fsl.add_skip_ext('abc')
        self.assertIn('abc', move_fsl.SKIP_EXTS)
        move_fsl.add_skip_ext('.cde')
        self.assertIn('cde', move_fsl.SKIP_EXTS)
        self.assertWarns(UserWarning, move_fsl.add_skip_ext, 'abc')
        self.assertWarns(UserWarning, move_fsl.add_skip_ext, '.abc')
        # Clean up
        move_fsl.SKIP_EXTS.remove('abc')
        move_fsl.SKIP_EXTS.remove('cde')
        
        move_fsl.add_skip_ext(['fgh'])
        self.assertIn('fgh', move_fsl.SKIP_EXTS)
        move_fsl.SKIP_EXTS.remove('fgh')
        move_fsl.add_skip_ext(('klm'))
        self.assertIn('klm', move_fsl.SKIP_EXTS)
        move_fsl.SKIP_EXTS.remove('klm')
        move_fsl.add_skip_ext(['nop', 'qrs' ])
        if 'nop' not in move_fsl.SKIP_EXTS and 'qrs' not in move_fsl.SKIP_EXTS:
            self.fail()
        move_fsl.SKIP_EXTS.remove('nop')
        move_fsl.SKIP_EXTS.remove('qrs')
        
    def test_remove_skip_ext(self):
        self.assertRaises(TypeError, move_fsl.remove_skip_ext, 0.0)
        self.assertRaises(TypeError, move_fsl.remove_skip_ext, 0)
        if 'abc' not in move_fsl.SKIP_EXTS:
            move_fsl.add_skip_ext('abc')
        move_fsl.remove_skip_ext('abc')
        self.assertNotIn('abc', move_fsl.SKIP_EXTS)
        self.assertWarns(UserWarning, move_fsl.remove_skip_ext, 'abc')
        move_fsl.add_skip_ext('abc')
        move_fsl.remove_skip_ext('.abc')
        self.assertNotIn('abc', move_fsl.SKIP_EXTS)
        
        move_fsl.add_skip_ext(['cde', 'fgh'])
        if 'cde' not in move_fsl.SKIP_EXTS and 'fgh' not in move_fsl.SKIP_EXTS:
            self.fail()
        move_fsl.remove_skip_ext(['cde', 'fgh'])
        if 'cde' in move_fsl.SKIP_EXTS and 'fgh' in move_fsl.SKIP_EXTS:
            self.fail()

    def test_add_skip_file(self):
        self.assertRaises(TypeError, move_fsl.add_skip_file, 0.0)
        self.assertRaises(TypeError, move_fsl.add_skip_file, 0)
        move_fsl.add_skip_file('abc')
        self.assertIn('abc', move_fsl.SKIP_FILES)
        self.assertWarns(UserWarning, move_fsl.add_skip_file, 'abc')
        move_fsl.SKIP_FILES.remove('abc')
        move_fsl.add_skip_file(['fgh'])
        self.assertIn('fgh', move_fsl.SKIP_FILES)
        move_fsl.SKIP_FILES.remove('fgh')
        move_fsl.add_skip_file(('klm'))
        self.assertIn('klm', move_fsl.SKIP_FILES)
        move_fsl.SKIP_FILES.remove('klm')
        move_fsl.add_skip_file(['nop', 'qrs' ])
        if 'nop' not in move_fsl.SKIP_FILES and 'qrs' not in move_fsl.SKIP_FILES:
            self.fail()
        move_fsl.SKIP_FILES.remove('nop')
        move_fsl.SKIP_FILES.remove('qrs')
        
    def test_remove_skip_file(self):
        self.assertRaises(TypeError, move_fsl.remove_skip_file, 0.0)
        self.assertRaises(TypeError, move_fsl.remove_skip_file, 0)
        if 'abc' not in move_fsl.SKIP_FILES:
            move_fsl.add_skip_file('abc')
        move_fsl.remove_skip_file('abc')
        self.assertNotIn('abc', move_fsl.SKIP_FILES)
        self.assertWarns(UserWarning, move_fsl.remove_skip_file, 'abc')
        move_fsl.add_skip_file(['cde', 'fgh'])
        if 'cde' not in move_fsl.SKIP_FILES and 'fgh' not in move_fsl.SKIP_FILES:
            self.fail()
        move_fsl.remove_skip_file(['cde', 'fgh'])
        if 'cde' in move_fsl.SKIP_FILES and 'fgh' in move_fsl.SKIP_FILES:
            self.fail()
        
    def test_add_fix_ext(self):
        self.assertRaises(TypeError, move_fsl.remove_fix_ext, 0.0)
        self.assertRaises(TypeError, move_fsl.remove_fix_ext, 0)
        move_fsl.add_fix_ext('abc')
        self.assertIn('abc', move_fsl.FIX_EXTS)
        move_fsl.add_fix_ext('.cde')
        self.assertIn('cde', move_fsl.FIX_EXTS)
        self.assertWarns(UserWarning, move_fsl.add_fix_ext, 'abc')
        self.assertWarns(UserWarning, move_fsl.add_fix_ext, '.abc')
        move_fsl.FIX_EXTS.remove('abc')
        move_fsl.FIX_EXTS.remove('cde')
        move_fsl.add_fix_ext(['fgh'])
        self.assertIn('fgh', move_fsl.FIX_EXTS)
        move_fsl.FIX_EXTS.remove('fgh')
        move_fsl.add_fix_ext(('klm'))
        self.assertIn('klm', move_fsl.FIX_EXTS)
        move_fsl.FIX_EXTS.remove('klm')
        move_fsl.add_fix_ext(['nop', 'qrs' ])
        if 'nop' not in move_fsl.FIX_EXTS and 'qrs' not in move_fsl.FIX_EXTS:
            self.fail()
        move_fsl.FIX_EXTS.remove('nop')
        move_fsl.FIX_EXTS.remove('qrs')
        
    def test_remove_fix_ext(self):
        self.assertRaises(TypeError, move_fsl.add_skip_ext, 0.0)
        self.assertRaises(TypeError, move_fsl.add_skip_ext, 0)
        if 'abc' not in move_fsl.FIX_EXTS:
            move_fsl.add_fix_ext('abc')
        move_fsl.remove_fix_ext('abc')
        self.assertNotIn('abc', move_fsl.FIX_EXTS)
        self.assertWarns(UserWarning, move_fsl.remove_fix_ext, 'abc')
        move_fsl.add_fix_ext('abc')
        move_fsl.remove_fix_ext('.abc')
        self.assertNotIn('abc', move_fsl.FIX_EXTS)
        move_fsl.add_fix_ext(['cde', 'fgh'])
        if 'cde' not in move_fsl.FIX_EXTS and 'fgh' not in move_fsl.FIX_EXTS:
            self.fail()
        move_fsl.remove_fix_ext(['cde', 'fgh'])
        if 'cde' in move_fsl.FIX_EXTS and 'fgh' in move_fsl.FIX_EXTS:
            self.fail()

    def test_add_fix_file(self):
        self.assertRaises(TypeError, move_fsl.add_fix_file, 0.0)
        self.assertRaises(TypeError, move_fsl.add_fix_file, 0)
        move_fsl.add_fix_file('abc')
        self.assertIn('abc', move_fsl.FIX_FILES)
        self.assertWarns(UserWarning, move_fsl.add_fix_file, 'abc')
        move_fsl.FIX_FILES.remove('abc')
        move_fsl.add_fix_file(['fgh'])
        self.assertIn('fgh', move_fsl.FIX_FILES)
        move_fsl.FIX_FILES.remove('fgh')
        move_fsl.add_fix_file(('klm'))
        self.assertIn('klm', move_fsl.FIX_FILES)
        move_fsl.FIX_FILES.remove('klm')
        move_fsl.add_fix_file(['nop', 'qrs' ])
        if 'nop' not in move_fsl.FIX_FILES and 'qrs' not in move_fsl.FIX_FILES:
            self.fail()
        move_fsl.FIX_FILES.remove('nop')
        move_fsl.FIX_FILES.remove('qrs')
        
        
    def test_remove_fix_file(self):
        self.assertRaises(TypeError, move_fsl.add_skip_ext, 0.0)
        self.assertRaises(TypeError, move_fsl.add_skip_ext, 0)
        if 'abc' not in move_fsl.FIX_FILES:
            move_fsl.add_fix_file('abc')
        move_fsl.remove_fix_file('abc')
        self.assertNotIn('abc', move_fsl.FIX_FILES)
        self.assertWarns(UserWarning, move_fsl.remove_fix_file, 'abc')
        move_fsl.add_fix_file(['cde', 'fgh'])
        if 'cde' not in move_fsl.FIX_FILES and 'fgh' not in move_fsl.FIX_FILES:
            self.fail()
        move_fsl.remove_fix_file(['cde', 'fgh'])
        if 'cde' in move_fsl.FIX_FILES and 'fgh' in move_fsl.FIX_FILES:
            self.fail()

class TestMoveFsl_misc(unittest.TestCase):
    def setUp(self):
        self.temp_dir_obj = tempfile.TemporaryDirectory()
        self.source_dir = self.temp_dir_obj.name
        self.path_source = Path(self.source_dir)
    def tearDown(self):
        self.temp_dir_obj.cleanup()
        
    def test_clone_fsobject(self):
        tfile = Path(self.path_source / 'afile')
        tfile2 = Path(self.path_source / 'bfile')
        random_textfile(tfile)
        time.sleep(2)
        random_textfile(tfile2)
        primary_g = os.getgroups()[0]
        sec_g = os.getgroups()[-1]
        if primary_g != sec_g:
            shutil.chown(str(tfile), -1, sec_g)
            shutil.chown(str(tfile2), -1, primary_g)
        move_fsl.clone_fsobject(tfile, tfile2)
        self.assertEqual(tfile.group(), tfile2.group())
        tf_stats = tfile.stat()
        tf2_stats = tfile2.stat()
        self.assertEqual(tf_stats.st_mode, tf2_stats.st_mode)
        self.assertEqual(tf_stats.st_atime, tf2_stats.st_atime)
        self.assertEqual(tf_stats.st_mtime, tf2_stats.st_mtime)
        other_dir = tempfile.TemporaryDirectory()
        other_dir_path = Path(other_dir.name)
        other_dir_path.chmod(stat.S_ISGID | stat.S_IRUSR | stat.S_IWUSR | stat.S_IXUSR )
        move_fsl.clone_fsobject(self.path_source, other_dir_path)
        td_stats = self.path_source.stat()
        td2_stats = other_dir_path.stat()
        self.assertEqual(td_stats.st_mode, td2_stats.st_mode)
        other_dir.cleanup()
    
    def test_ftype(self):
        tf = tempfile.NamedTemporaryFile(delete=False)
        tf_name = tf.name
        tf.close()
        self.assertEqual(move_fsl.FILE, move_fsl.ftype(Path(tf_name)))
        td = tempfile.TemporaryDirectory()
        self.assertEqual(move_fsl.DIRECTORY, move_fsl.ftype(Path(td.name)))
        tl = Path(td.name) / 'asymlink'
        tl.symlink_to(tf_name)
        self.assertEqual(move_fsl.SYMLINK, move_fsl.ftype(Path(tl)))
        tl.unlink()
        tl.symlink_to('/dev/null')
        with patch.object(move_fsl.Path, 'is_symlink', autospec=True) as mock_symlink:
            mock_symlink.return_value = False
            with self.assertRaises(TypeError) as error:
                move_fsl.ftype(Path(tl))
        the_exception = error.exception
        if not str(the_exception).endswith("Unexpected filesystem object type"):
            self.fail(str(the_exception))
        Path(tf_name).unlink()
        td.cleanup()
        
class TestMoveFsl_parseargs(unittest.TestCase):
    def setUp(self):
        self.temp_dir_obj = tempfile.TemporaryDirectory()
        self.source_dir = self.temp_dir_obj.name
        os.mkdir(os.path.join(self.source_dir, 'source'))
        
    def tearDown(self):
        self.temp_dir_obj.cleanup()
        
    def test_move(self):
        options = move_fsl.parse_args(['source', 'destination'])
        source = options.source[0]
        self.assertEqual(source, 'source')
        self.assertEqual(options.destination, 'destination')

    def test_multiple_move(self):
        folders = ['source1', 'source2',]
        for folder in folders:
            os.mkdir(os.path.join(self.source_dir, folder))
        folders.append('destination')
        options = move_fsl.parse_args(folders)
        self.assertEqual(options.source[0], 'source1')
        self.assertEqual(options.source[1], 'source2')
        self.assertEqual(options.destination, 'destination')

    def test_missing_destination(self):
        f = io.StringIO()
        with redirect_stderr(f):
            self.assertRaises(SystemExit, move_fsl.parse_args, ['source'] )
        self.assertIn('error: No source/destination specified', f.getvalue())
        
    def test_copy(self):
        options = move_fsl.parse_args(['-c', 'source', 'destination'])
        self.assertTrue(options.copy)
        source = options.source[0]
        self.assertEqual(source, 'source')
        self.assertEqual(options.destination, 'destination')

    def test_debug(self):
        options = move_fsl.parse_args(['-d', 'source', 'destination'])
        self.assertTrue(options.debug)

    def test_verbose(self):
        options = move_fsl.parse_args(['-v', 'source', 'destination'])
        self.assertTrue(options.verbose)

class TestMoveFsl_validate_input(unittest.TestCase):
    @patch('move_fsl.ask_user_confirm', auto_spec=True)
    def test_target_slash_source_exists(self, mock_confirm):
        td = tempfile.TemporaryDirectory()
        sd = tempfile.TemporaryDirectory()
        tf = tempfile.NamedTemporaryFile(dir=sd.name)
        tfp = Path(tf.name)
        tdp = Path(td.name)
        sdp = Path(sd.name)
        os.mkdir(str(tdp / sdp.name))
        mock_confirm.return_value = False
        self.assertRaises(FileExistsError, move_fsl.validate_input, sdp, tdp)
        self.assertEqual(mock_confirm.call_count,1)
        tf.close()
        td.cleanup()
        sd.cleanup()
        
    @patch('move_fsl.ask_user_confirm', auto_spec=True)
    def test_target_file_exists(self, mock_confirm):
        td = tempfile.TemporaryDirectory()
        sd = tempfile.TemporaryDirectory()
        tf = tempfile.NamedTemporaryFile(dir=sd.name)
        tfp = Path(tf.name)
        tdp = Path(td.name)
        sdp = Path(sd.name)
        target_file = tdp / tfp.name
        target_file.write_text("Hello")
        mock_confirm.return_value = False
        self.assertRaises(FileExistsError, move_fsl.validate_input, tfp, target_file)
        self.assertEqual(mock_confirm.call_count,1)
        tf.close()
        td.cleanup()
        sd.cleanup()
    
    def test_source_missing(self):
        tf = tempfile.NamedTemporaryFile()
        tp = Path(tf.name)
        tf.close()
        self.assertRaises(FileNotFoundError, move_fsl.validate_input, tp, Path('/usr/local'))
        self.assertRaises(FileNotFoundError, move_fsl.validate_input, [tp], Path('/usr/local'))
        
    def test_destination_missing(self):
        tps = []
        tfs = []
        for i in [1, 2]:
            tf = tempfile.NamedTemporaryFile()
            tfs.append(tf)
            tps.append(Path(tf.name))
        td = tempfile.NamedTemporaryFile()
        tdp = Path(td.name)
        td.close()
        # Multiple sources, no pre-existing folder is an error
        self.assertRaises(FileNotFoundError, move_fsl.validate_input, tps, tdp)
        # Single source, no pre-existing folder is not an error
        td = tempfile.TemporaryDirectory()
        tdp = Path(td.name)
        move_fsl.validate_input(tps[0], tdp / 'atarget' )
        # Not being able to write to the parent of the target is an error
        os.chmod(str(tdp), stat.S_IRUSR | stat.S_IXUSR)
        self.assertRaises(PermissionError, move_fsl.validate_input, tps[0], tdp / 'atarget')
        os.chmod(str(tdp), stat.S_IRUSR | stat.S_IWUSR | stat.S_IXUSR)
        for t in tfs:
            t.close()
        td.cleanup()
    
    @patch('move_fsl.ask_user_confirm', auto_spec=True)
    def test_source_file_target_directory(self, mock_confirm):
        # Target is a pre-existing directory, source is a single file
        tf = tempfile.NamedTemporaryFile()
        tfp = Path(tf.name)
        td = tempfile.TemporaryDirectory()
        tdp = Path(td.name)
        
        move_fsl.validate_input(tfp, tdp)
        # No writing to the target directory
        os.chmod(td.name, stat.S_IRUSR | stat.S_IXUSR)
        self.assertRaises(PermissionError, move_fsl.validate_input, tfp, tdp)
        os.chmod(td.name, stat.S_IRUSR | stat.S_IWUSR | stat.S_IXUSR)
        
        # target/source already exists as a file
        target_file = tdp / tfp.name
        target_file.write_text("Hello")
        mock_confirm.return_value=True
        move_fsl.validate_input( tfp, tdp)
        mock_confirm.assert_called_once_with('{} exists, do you want to overwrite? [y|n]:'.format(target_file))
        # ...and user requests no overwriting
        mock_confirm.reset_mock()
        mock_confirm.return_value = False
        self.assertRaises(FileExistsError, move_fsl.validate_input, tfp, tdp)
        mock_confirm.assert_called_once_with('{} exists, do you want to overwrite? [y|n]:'.format(target_file))
        self.assertEqual(mock_confirm.call_count, 1)
        # ...and script requests overwriting
        mock_confirm.reset_mock()
        mock_confirm.return_value = False
        move_fsl.validate_input(tfp, tdp, force=True)
        self.assertEqual(mock_confirm.call_count, 0)
        td.cleanup()
        tf.close()

    def test_source_badinput(self):
        td = tempfile.TemporaryDirectory()
        tdp = Path(td.name)
        tl = tdp / 'asymlink'
        tl.symlink_to('/usr/local')
        with self.assertRaises(move_fsl.NotAFileOrDirectoryError) as error:
            move_fsl.validate_input(tl, tdp / 'anotherlink')
        
        the_exception = error.exception
        if not str(the_exception).endswith("asymlink is not a file or directory"):
            self.fail(str(the_exception))
        with patch.object(move_fsl.Path, 'is_file', auto_spec=True, return_value=False):
            with patch.object(move_fsl.Path, 'is_dir', auto_spec=True, return_value=False):
                with patch.object(move_fsl.Path, 'is_symlink', auto_spec=True, return_value=False):
                    with self.assertRaises(TypeError) as error:
                        move_fsl.validate_input(tl, tdp / 'anotherlink')
        
        the_exception = error.exception
        if not str(the_exception).endswith("asymlink is not a file or directory"):
            self.fail(str(the_exception))
        td.cleanup()
    
    def test_source_folder_permissiondenied(self):
        td = tempfile.TemporaryDirectory()
        dd = tempfile.TemporaryDirectory()
        os.chmod(td.name, 0)
        self.assertRaises(PermissionError,
                          move_fsl.validate_input,
                          Path(td.name),
                          Path(dd.name))
        os.chmod(td.name, stat.S_IRUSR | stat.S_IWUSR | stat.S_IXUSR)
        td.cleanup()
        dd.cleanup()
    
    def test_source_file_permissiondenied(self):
        tf = tempfile.NamedTemporaryFile()
        dd = tempfile.TemporaryDirectory()
        os.chmod(tf.name, 0)
        self.assertRaises(PermissionError,
                          move_fsl.validate_input,
                          Path(tf.name),
                          Path(dd.name))
        os.chmod(tf.name, stat.S_IRUSR | stat.S_IWUSR)
        tf.close()
        dd.cleanup()

    def test_wrong_type(self):
        self.assertRaises(TypeError, move_fsl.validate_input,'a', 'b')
        self.assertRaises(TypeError, move_fsl.validate_input, ['a'], 'b')
        self.assertRaises(TypeError, move_fsl.validate_input, 0, 0)
        self.assertRaises(TypeError, move_fsl.validate_input, 0.0, 0.0)
    
    def test_destination_folder_permissiondenied(self):
        td = tempfile.TemporaryDirectory()
        tf = tempfile.NamedTemporaryFile()
        tfp = Path(tf.name)
        os.chmod(td.name, 0)
        self.assertRaises(PermissionError,
                          move_fsl.validate_input,
                          tfp, Path(td.name))
        os.chmod(td.name, stat.S_IRUSR | stat.S_IWUSR | stat.S_IXUSR)
        tf.close()
        td.cleanup()
        
    def test_destination_parent_permissiondenied(self):
        td = tempfile.TemporaryDirectory()
        tf = tempfile.NamedTemporaryFile()
        tfp = Path(tf.name)
        os.chmod(td.name, stat.S_IRUSR | stat.S_IXUSR )
        self.assertRaises(PermissionError,
                          move_fsl.validate_input,
                          tfp,
                          Path(td.name) / "target")
        os.chmod(td.name, stat.S_IRUSR | stat.S_IWUSR | stat.S_IXUSR)
        tf.close()
        td.cleanup()
    
    def test_destination_in_cwd(self):
        td = tempfile.TemporaryDirectory()
        tf = tempfile.NamedTemporaryFile()
        tfp = Path(tf.name)
        name = td.name
        dir = os.curdir
        os.chdir(name)
        with open('source', 'w') as f:
            f.write('Hello')
        with open('source2', 'w') as f:
            f.write('Hello')
        self.assertRaises(NotADirectoryError, 
                          move_fsl.validate_input, 
                          [ Path('source'), Path('source2'), ], 
                          tfp, force=True)
        os.remove('source')
        os.remove('source2')
        os.chdir(dir)
        tf.close()
        td.cleanup()

def random_textfile(pathobj, set_gp=True):
    test_text = ''.join(random.choice(string.ascii_uppercase + string.digits) for _ in range(5))
    pathobj.write_text(test_text)
    # File creation in the temp folder structure sets group to '0'!
    if set_gp:
        shutil.chown(str(pathobj), -1, os.getgroups()[0])
    return test_text

def random_binfile(pathobj, set_gp=True):
    pathobj.write_bytes(os.urandom(1024))
    # File creation in the temp folder structure sets group to '0'!
    if set_gp:
        shutil.chown(str(pathobj), -1, os.getgroups()[0])
    
class TestCopyFile(unittest.TestCase):
    def setUp(self):
        self.source_dir_obj = tempfile.TemporaryDirectory()
        self.source_dir = self.source_dir_obj.name
        self.source_name = 'source_folder'
        self.destination_name = 'target_folder'
        self.source_path = os.path.join(self.source_dir, self.source_name )
        self.destination_path = os.path.join(self.source_dir, self.destination_name )
        self.path_source = Path(self.source_path)
        self.path_destination = Path(self.destination_path)
        
    def tearDown(self):
        self.source_dir_obj.cleanup()

    def test_copy_empty_file(self):
        testfile = 'emptyfile'
        self.path_source.mkdir()
        self.path_destination.mkdir()
        dst_file = self.path_destination / testfile
        src_file = self.path_source / testfile
        src_file.touch()
        with patch.object(move_fsl, '_copy_file', autospec=True) as mock__copy_file:
            with patch.object(move_fsl, 'clean_file', autospec=True) as mock_clean_file:
                mock_clean_file.return_value = None
                mock__copy_file.return_value = None
                move_fsl.copy_file(src_file, dst_file, self.path_source, self.path_destination)
        mock__copy_file.assert_called_once_with(src_file, dst_file)
        if mock_clean_file.called:
            self.fail("Shouldn't call clean file on an empty file")
        
    def test__copy_file_preservegroup(self):
        test_file = 'testfile'
        self.path_source.mkdir()
        self.path_destination.mkdir()
        dst_file = self.path_destination / test_file
        source_file = self.path_source / test_file
        random_binfile(source_file)
        
        user_groups = [grp.getgrgid(g).gr_name for g in os.getgroups()]
        system_groups = [g.gr_name for g in grp.getgrall()]
        test_group = [g for g in system_groups if g not in user_groups][-1]
        test_u_group = user_groups[-1]
        if source_file.group() == test_u_group:
            if len(user_groups) > 1:
                test_u_group = user_groups[0]
            else:
                return self.skip("User only in one group to unable to test group preservation")
        with patch.object(move_fsl.Path, 'group', side_effect=[test_group, test_u_group], autospec=True) as mock_method:
            move_fsllogger = logging.getLogger('move_fsl')
            with patch.object(move_fsllogger, 'warning', autospec=True) as mock_logger: 
                move_fsl._copy_file(source_file, dst_file)
                mock_logger.assert_called_once_with("Not setting group on {0} as not a member of group {1}".format(str(dst_file),test_group))
        dst_file.unlink()
        shutil.chown(str(source_file), -1, test_u_group)
        with patch.object(move_fsl.Path, 'group', return_value=test_u_group, autospec=True) as mock_method:
            move_fsl._copy_file(source_file, dst_file)
            self.assertEqual(dst_file.group(), test_u_group)
        # Test failure to set group
        
        with patch.object(move_fsl.Path, 'group', side_effect=[test_group, test_u_group], autospec=True) as mock_method:
            move_fsllogger = logging.getLogger('move_fsl')
            with patch.object(move_fsl, 'clone_fsobject', side_effect=PermissionError("testerror"), autospec=True) as mock_clone:
                with patch.object(move_fsllogger, 'error', autospec=True) as mock_logger: 
                    move_fsl._copy_file(source_file, dst_file)
            mock_logger.assert_called_once_with("PermissionError: testerror - group of file {0} not changed".format(str(dst_file)))
            self.assertNotEqual(dst_file.group(), source_file.group())
    
    def test__copy_file(self):
        test_file = 'testfile'
        dst_file = self.path_destination / test_file
        source_file = self.path_source / test_file
        self.path_source.mkdir()
        self.path_destination.mkdir()
        test_text = random_textfile(source_file)
        move_fsl._copy_file(source_file, dst_file)
        
        content=dst_file.read_text()
        self.assertEqual(test_text, content)
        self.assertEqual(source_file.group(), dst_file.group())
        self.assertEqual(source_file.stat().st_atime, dst_file.stat().st_atime)
        self.assertEqual(source_file.stat().st_mtime, dst_file.stat().st_mtime)

    def test_copy_file_skip(self):
        test_file = list(move_fsl.SKIP_FILES)[0]
        dst_file = self.path_destination / test_file
        source_file = self.path_source / test_file
        
        with patch.object(move_fsl, '_copy_file', autospec=True) as mock__copy_file:
            move_fsl.copy_file(source_file, dst_file, self.path_source, self.path_destination)
        mock__copy_file.assert_called_once_with(source_file, dst_file)
         
    def test_copy_ext_skip(self):
        test_file = "afile." + list(move_fsl.SKIP_EXTS)[0]
        dst_file = self.path_destination / test_file
        source_file = self.path_source / test_file
        with patch.object(move_fsl, '_copy_file', autospec=True) as mock__copy_file:
            move_fsl.copy_file(source_file, dst_file, self.path_source, self.path_destination)
        mock__copy_file.assert_called_once_with(source_file, dst_file)
    
    def test_copy_file_fix(self):
        test_file = list(move_fsl.FIX_FILES)[0]
        dst_file = self.path_destination / test_file
        source_file = self.path_source / test_file
        
        with patch.object(move_fsl, 'clean_file', autospec=True) as mock_clean_file:
            move_fsl.copy_file(source_file, dst_file, self.path_source, self.path_destination)
        mock_clean_file.assert_called_once_with(source_file, dst_file, self.path_source, self.path_destination)
         
    def test_copy_ext_fix(self):
        test_file = "afile." + list(move_fsl.FIX_EXTS)[0]
        dst_file = self.path_destination / test_file
        source_file = self.path_source / test_file
        with patch.object(move_fsl, 'clean_file', autospec=True) as mock_clean_file:
            move_fsl.copy_file(source_file, dst_file, self.path_source, self.path_destination)
        mock_clean_file.assert_called_once_with(source_file, dst_file, self.path_source, self.path_destination)
        
    def test_copy_file_text(self):
        test_file = 'testfile'
        dst_file = self.path_destination / test_file
        source_file = self.path_source / test_file
        self.path_source.mkdir()
        self.path_destination.mkdir()
        random_textfile(source_file)
        with patch.object(move_fsl, 'clean_file', autospec=True) as mock_clean_file:
            move_fsl.copy_file(source_file, dst_file, self.path_source, self.path_destination)
        mock_clean_file.assert_called_once_with(source_file, dst_file, self.path_source, self.path_destination)
    
    def test_copy_file_binary(self):
        test_file = 'testfile' 
        dst_file = self.path_destination / test_file
        source_file = self.path_source / test_file
        self.path_source.mkdir()
        self.path_destination.mkdir()
        random_binfile(source_file)
        with patch.object(move_fsl, '_copy_file', autospec=True) as mock__copy_file:
            move_fsl.copy_file(source_file, dst_file, self.path_source, self.path_destination)
        mock__copy_file.assert_called_once_with(source_file, dst_file)
    
    def test_move_file(self):
        test_file = 'testfile' 
        dst_file = self.path_destination / test_file
        source_file = self.path_source / test_file
        with patch.object(move_fsl, 'copy_file', autospec=True) as mock_copy_file:
            mock_copy_file.return_value = None
            with patch.object(move_fsl.Path, 'unlink', autospec=True) as mock_unlink:
                move_fsl.move_file(source_file, dst_file, self.path_source, self.path_destination)
            mock_unlink.assert_called_once_with(source_file)
        mock_copy_file.assert_called_once_with(source_file, dst_file, self.path_source, self.path_destination)

    def test_move_file_fails(self):
        test_file = 'testfile' 
        dst_file = self.path_destination / test_file
        source_file = self.path_source / test_file
        with patch.object(move_fsl, 'copy_file', autospec=True) as mock_copy_file:
            mock_copy_file.return_value = Exception("An error")
            with patch.object(move_fsl.Path, 'unlink', autospec=True) as mock_unlink:
                error = move_fsl.move_file(source_file, dst_file, self.path_source, self.path_destination)
            self.assertEqual(mock_unlink.call_count, 0)
        self.assertIsInstance(error, Exception)

class TestCopyFileRewrite(unittest.TestCase):
    def setUp(self):
        self.source_dir_obj = tempfile.TemporaryDirectory()
        self.source_dir = self.source_dir_obj.name
        self.source_name = 'source_folder'
        self.destination_name = 'target_folder'
        self.source_path = os.path.join(self.source_dir, self.source_name )
        self.destination_path = os.path.join(self.source_dir, self.destination_name )
        self.path_source = Path(self.source_path)
        self.path_destination = Path(self.destination_path)
        
    def tearDown(self):
        self.source_dir_obj.cleanup()

    def test_donothing(self):
        test_file = 'testfile'
        dst_file = self.path_destination / test_file
        source_file = self.path_source / test_file
        self.path_source.mkdir()
        self.path_destination.mkdir()
        src_content = random_textfile(source_file)
        move_fsl.copy_file_rewrite(source_file, dst_file, (('/usr/local/blah', '/opt/local/blah'),))
        self.assertTrue(dst_file.exists(), "File not copied")
        self.assertTrue(dst_file.is_file())
        self.assertEqual(dst_file.read_text(), src_content, "Content differs")
        src_stat = source_file.stat()
        dst_stat = dst_file.stat()
        self.assertEqual(dst_stat.st_uid, src_stat.st_uid, "Owner differs")
        self.assertEqual(dst_stat.st_gid, src_stat.st_gid, "Group differs")
        self.assertEqual(dst_stat.st_atime, src_stat.st_atime, "atime differs")
        self.assertEqual(dst_stat.st_mtime, src_stat.st_mtime, "mtime differs")
        self.assertEqual(dst_stat.st_mode, src_stat.st_mode, "permissions differ")
    
    def test_multiple_changes_donothing(self):
        test_file = 'testfile'
        dst_file = self.path_destination / test_file
        source_file = self.path_source / test_file
        self.path_source.mkdir()
        self.path_destination.mkdir()
        src_content = random_textfile(source_file)
        move_fsl.copy_file_rewrite(source_file, dst_file, (('/usr/local/blah', '/opt/local/blah'), ('/some/path', '/someother/path')))
        self.assertTrue(dst_file.exists(), "File not copied")
        self.assertTrue(dst_file.is_file())
        self.assertEqual(dst_file.read_text(), src_content, "Content differs")
        src_stat = source_file.stat()
        dst_stat = dst_file.stat()
        self.assertEqual(dst_stat.st_uid, src_stat.st_uid, "Owner differs")
        self.assertEqual(dst_stat.st_gid, src_stat.st_gid, "Group differs")
        self.assertEqual(dst_stat.st_atime, src_stat.st_atime, "atime differs")
        self.assertEqual(dst_stat.st_mtime, src_stat.st_mtime, "mtime differs")
        self.assertEqual(dst_stat.st_mode, src_stat.st_mode, "permissions differ")

    def test_clean(self):
        test_file = 'testfile'
        dst_file = self.path_destination / test_file
        source_file = self.path_source / test_file
        self.path_source.mkdir()
        self.path_destination.mkdir()
        src_content = "somedata: /usr/local/blah/somedata"
        dst_content = src_content.replace('/usr/local/blah', '/opt/local/blah')
        source_file.write_text(src_content)
        move_fsl.copy_file_rewrite(source_file, dst_file, (('/usr/local/blah', '/opt/local/blah'),))
        self.assertTrue(dst_file.exists(), "File not copied")
        self.assertTrue(dst_file.is_file())
        self.assertEqual(dst_file.read_text(), dst_content, "Content differs")
        src_stat = source_file.stat()
        dst_stat = dst_file.stat()
        self.assertEqual(dst_stat.st_uid, src_stat.st_uid, "Owner differs")
        self.assertEqual(dst_stat.st_gid, src_stat.st_gid, "Group differs")
        self.assertEqual(dst_stat.st_atime, src_stat.st_atime, "atime differs")
        self.assertEqual(dst_stat.st_mtime, src_stat.st_mtime, "mtime differs")
        self.assertEqual(dst_stat.st_mode, src_stat.st_mode, "permissions differ")

class TestCleanFile(unittest.TestCase):
    def setUp(self):
        self.source_dir_obj = tempfile.TemporaryDirectory()
        self.source_dir = self.source_dir_obj.name
        self.source_name = 'source_folder'
        self.destination_name = 'target_folder'
        self.source_path = os.path.join(self.source_dir, self.source_name )
        self.destination_path = os.path.join(self.source_dir, self.destination_name )
        self.path_source = Path(self.source_path)
        self.path_destination = Path(self.destination_path)
        
    def tearDown(self):
        self.source_dir_obj.cleanup()

    def test_nothing_to_do(self):
        test_file = 'testfile'
        dst_file = self.path_destination / test_file
        source_file = self.path_source / test_file
        self.path_source.mkdir()
        self.path_destination.mkdir()
        src_content = random_textfile(source_file)
        move_fsl.clean_file(source_file, dst_file, source_file, dst_file)
        self.assertTrue(dst_file.exists(), "File not copied")
        self.assertTrue(dst_file.is_file())
        self.assertEqual(dst_file.read_text(), src_content, "Content differs")
        src_stat = source_file.stat()
        dst_stat = dst_file.stat()
        self.assertEqual(dst_stat.st_uid, src_stat.st_uid, "Owner differs")
        self.assertEqual(dst_stat.st_gid, src_stat.st_gid, "Group differs")
        self.assertEqual(dst_stat.st_atime, src_stat.st_atime, "atime differs")
        self.assertEqual(dst_stat.st_mtime, src_stat.st_mtime, "mtime differs")
        self.assertEqual(dst_stat.st_mode, src_stat.st_mode, "permissions differ")

    def test_rewrite_path(self):
        test_file = 'testfile'
        dst_file = self.path_destination / test_file
        source_file = self.path_source / test_file
        self.path_source.mkdir()
        self.path_destination.mkdir()
        src_content = random_textfile(source_file)
        dst_content = src_content
        src_content += str(source_file)
        dst_content += str(dst_file)
        source_file.write_text(src_content)
        move_fsl.clean_file(source_file, dst_file, source_file, dst_file)
        self.assertTrue(dst_file.exists(), "File not copied")
        self.assertTrue(dst_file.is_file())
        self.assertNotEqual(dst_file.read_text(), src_content, "Content doesn't differ")
        self.assertEqual(dst_file.read_text(), dst_content, "Content not as expected")
        src_stat = source_file.stat()
        dst_stat = dst_file.stat()
        self.assertEqual(dst_stat.st_uid, src_stat.st_uid, "Owner differs")
        self.assertEqual(dst_stat.st_gid, src_stat.st_gid, "Group differs")
        self.assertEqual(dst_stat.st_atime, src_stat.st_atime, "atime differs")
        self.assertEqual(dst_stat.st_mtime, src_stat.st_mtime, "mtime differs")
        self.assertEqual(dst_stat.st_mode, src_stat.st_mode, "permissions differ")
        
    def test_rewrite_path_nested(self):
        test_file = 'testfile'
        sub_folder = 'subfolder'
        dst_file = self.path_destination / sub_folder / test_file
        source_file = self.path_source / sub_folder / test_file
        self.path_source.mkdir()
        self.path_destination.mkdir()
        (self.path_source / sub_folder).mkdir()
        (self.path_destination / sub_folder).mkdir()
        src_content = random_textfile(source_file)
        dst_content = src_content
        src_content += os.path.join(str(self.path_source),'anotherfile')
        dst_content += os.path.join(str(self.path_destination), 'anotherfile')
        source_file.write_text(src_content)
        move_fsl.clean_file(source_file, dst_file, self.path_source, self.path_destination)
        self.assertTrue(dst_file.exists(), "File not copied")
        self.assertTrue(dst_file.is_file())
        self.assertNotEqual(dst_file.read_text(), src_content, "Content doesn't differ")
        self.assertEqual(dst_file.read_text(), dst_content, "Content not as expected")
        src_stat = source_file.stat()
        dst_stat = dst_file.stat()
        self.assertEqual(dst_stat.st_uid, src_stat.st_uid, "Owner differs")
        self.assertEqual(dst_stat.st_gid, src_stat.st_gid, "Group differs")
        self.assertEqual(dst_stat.st_atime, src_stat.st_atime, "atime differs")
        self.assertEqual(dst_stat.st_mtime, src_stat.st_mtime, "mtime differs")
        self.assertEqual(dst_stat.st_mode, src_stat.st_mode, "permissions differ")
    
    def test_cant_change_group(self):
        test_file = 'testfile'
        dst_file = self.path_destination / test_file
        source_file = self.path_source / test_file
        self.path_source.mkdir()
        self.path_destination.mkdir()
        src_content = random_textfile(source_file)
        with patch.object(move_fsl, 'group_membership', autospec=True) as mock_groupmemb:
            mock_groupmemb.return_value(['arandomgroup',])
            with patch.object(move_fsl.Path, 'group', autospec=True) as mock_group:
                mock_group.side_effect = ['groupa', 'groupb']
                move_fsllogger = logging.getLogger('move_fsl')
                with patch.object(move_fsllogger, 'warning', autospec=True) as mock_logger:
                    move_fsl.clean_file(source_file, dst_file, self.path_source, self.path_destination)
                    mock_logger.assert_called_once_with("Not setting group on {0} as not a member of group {1}".format(str(dst_file), 'groupa'))
        self.assertTrue(dst_file.exists(), "File not copied")
        self.assertTrue(dst_file.is_file())
        self.assertEqual(dst_file.read_text(), src_content, "Content differs")
        src_stat = source_file.stat()
        dst_stat = dst_file.stat()
        self.assertEqual(dst_stat.st_uid, src_stat.st_uid, "Owner differs")
        self.assertEqual(dst_stat.st_atime, src_stat.st_atime, "atime differs")
        self.assertEqual(dst_stat.st_mtime, src_stat.st_mtime, "mtime differs")
        self.assertEqual(dst_stat.st_mode, src_stat.st_mode, "permissions differ")
    
    def test_alias_rewrite(self):
        test_file = 'testfile'
        dst_file = self.path_destination / test_file
        src_file = self.path_source / test_file
        self.path_source.mkdir()
        self.path_destination.mkdir()
        src_content = '''mydata = /home/fs0/jbloggs/scratch/afile
myotherdata = /vols/Scratch/jbloggs/bfile'''
        src_file.write_text(src_content)
        with patch.object(move_fsl, 'get_username', autospec=True, return_value='jbloggs') as mock_gu:
            move_fsl.clean_file(src_file, dst_file, Path('/vols/Scratch/jbloggs/afile'), Path('/vols/Data/jbloggs/afile'))
        dst_content = dst_file.read_text()
        self.assertEqual(dst_content, '''mydata = /vols/Data/jbloggs/afile
myotherdata = /vols/Scratch/jbloggs/bfile''')
        
class TestFileMagic(unittest.TestCase):
    def setUp(self):
        self.source_dir_obj = tempfile.TemporaryDirectory()
        self.source_dir = self.source_dir_obj.name
        self.source_name = 'source_folder'
        self.destination_name = 'target_folder'
        self.source_path = os.path.join(self.source_dir, self.source_name )
        self.destination_path = os.path.join(self.source_dir, self.destination_name )
        self.path_source = Path(self.source_path)
        self.path_destination = Path(self.destination_path)
        self.path_source.mkdir()
        self.path_destination.mkdir()
        self.orig_import = __import__
        
    def tearDown(self):
        self.source_dir_obj.cleanup()
        
    def test_file_encoding(self):
        source_file = self.path_source / "afile"
        src_content = random_textfile(source_file)
        
        if NO_MAGIC:
            with patch.object(move_fsl.sp, "run", autospec=True) as sp_mock:
                sp_mock.return_value = sp.CompletedProcess(args='',
                                                           returncode = 0,
                                                           stdout='us-ascii',
                                                           stderr='')
                encoding = move_fsl.get_fileencoding(source_file)
            sp_mock.assert_called_once_with(['/usr/bin/file', '-b', '--mime-encoding', str(source_file)],
                             stdout=sp.PIPE, stderr=sp.PIPE,
                             universal_newlines=True)
        else:
            with patch.object(move_fsl.magic.Magic, autospec=True) as magic_magic_mock:
                move_fsl.get_fileencoding(source_file)
                magic_magic_mock.assert_called_once_with(flags=magic.MAGIC_MIME_ENCODING)
                with patch.object(movel_fsl.get_fileencoding.m, 'id_filename', autospec=True) as magic_obj_mock:
                    move_fsl.get_fileencoding(source_file)
                magic_obj_mock.assert_called_once_with(str(source_file))
        self.assertEqual('us-ascii', move_fsl.get_fileencoding(source_file))
        
    def test_file_istext(self):
        source_file = self.path_source / "afile"
        src_content = random_textfile(source_file)
        self.assertTrue(move_fsl.file_istext(source_file))
        random_binfile(source_file)
        self.assertFalse(move_fsl.file_istext(source_file))
        
class TestCopyLink(unittest.TestCase):
    def setUp(self):
        self.source_dir_obj = tempfile.TemporaryDirectory()
        self.source_dir = self.source_dir_obj.name
        self.source_name = 'source_folder'
        self.destination_name = 'target_folder'
        self.source_path = os.path.join(self.source_dir, self.source_name )
        self.destination_path = os.path.join(self.source_dir, self.destination_name )
        self.path_source = Path(self.source_path)
        self.path_destination = Path(self.destination_path)
        self.test_link = 'alink'
        self.dst_link = self.path_destination / self.test_link
        self.src_link = self.path_source / self.test_link
        self.path_source.mkdir()
        self.path_destination.mkdir()
        self.move_fsllogger = logging.getLogger('move_fsl')

        
    def tearDown(self):
        self.source_dir_obj.cleanup()
    
    def test_absolute(self):
        self.src_link.symlink_to('/usr/local')
        move_fsl.copy_link(self.src_link, self.dst_link, self.path_source, self.path_destination)
        self.assertEqual(str(self.dst_link.resolve()), '/usr/local')
    
    def test_relative(self):
        rel_link = os.path.join('..', self.source_name)
        self.src_link.symlink_to(rel_link)
        move_fsl.copy_link(self.src_link, self.dst_link, self.path_source, self.path_destination)
        # resolve will resolve it to an absolute link
        self.assertEqual(os.readlink(str(self.dst_link)), str(rel_link))
        
    def test_dest_exists(self):
        self.src_link.symlink_to('/usr/local')
        self.dst_link.symlink_to('/usr/local')
        
        # Testing destination link already exists
        with patch.object(self.move_fsllogger, 'error',autospec=True) as mock_logger:
            error = move_fsl.copy_link(self.src_link, self.dst_link, self.path_source, self.path_destination)
        mock_logger.assert_called_once_with("FileExistsError: {0} on creating link {1}".format(str(error), str(self.dst_link)))
        self.assertEqual(str(error), "[Errno 17] File exists: '{0}' -> '{1}'".format(os.readlink(str(self.src_link)), str(self.dst_link)))
    
    def test_dest_permissiondenied(self):
        # Testing permission denied
        self.src_link.symlink_to('/usr/local')
        self.path_destination.chmod(0)
        with patch.object(self.move_fsllogger, 'error',autospec=True) as mock_logger: 
            error = move_fsl.copy_link(self.src_link, self.dst_link, self.path_source, self.path_destination)
        mock_logger.assert_called_once_with("PermissionError: {0} on creating link {1}".format(str(error), str(self.dst_link)))
        self.assertEqual(str(error), "[Errno 13] Permission denied: '{0}' -> '{1}'".format(os.readlink(str(self.src_link)), str(self.dst_link)))
        self.path_destination.chmod(stat.S_IRUSR | stat.S_IWUSR | stat.S_IXUSR)
        
    def test_link_rewrite(self):
        # Testing link re-writing
        self.src_link.symlink_to(self.path_source / "anotherfile" )
        with patch.object(self.move_fsllogger, 'debug',autospec=True) as mock_logger: 
            error = move_fsl.copy_link(self.src_link, self.dst_link, self.path_source, self.path_destination)
        mock_logger.assert_has_calls([ call("Requested copy of symlink {0} to {1} fixing as necessary".format(str(self.src_link), str(self.dst_link))),
                                      call("Re-writing {0} to {1}".format(str(self.path_source), str(self.path_destination))), ])
        self.assertIsNone(error)
        target_link = self.dst_link.parent / "anotherfile"
        self.assertEqual(os.readlink(str(self.dst_link)),str(target_link))

class TestMoveLink(unittest.TestCase):
    def test_move_success(self):
        with patch.object(move_fsl, 'copy_link', autospec=True) as mock_copy_link:
            mock_copy_link.return_value = None
            with patch.object(move_fsl.Path, 'unlink') as mock_unlink:
                error = move_fsl.move_link(Path('/usr/local/alink'), Path('/opt/blink'), Path('/usr/local/alink'), Path('/opt/blink'))
        mock_copy_link.assert_called_once_with(Path('/usr/local/alink'), Path('/opt/blink'), Path('/usr/local/alink'), Path('/opt/blink'))
        mock_unlink.assert_called_once_with()
        self.assertIsNone(error)
    
    def test_move_failure(self):
        with patch.object(move_fsl, 'copy_link', autospec=True) as mock_copy_link:
            mock_copy_link.return_value = PermissionError()
            with patch.object(move_fsl.Path, 'unlink') as mock_unlink:
                error = move_fsl.move_link(Path('/usr/local/alink'), Path('/opt/blink'), Path('/usr/local/alink'), Path('/opt/blink'))
        mock_copy_link.assert_called_once_with(Path('/usr/local/alink'), Path('/opt/blink'), Path('/usr/local/alink'), Path('/opt/blink'))
        mock_unlink.assert_not_called()
        self.assertIsInstance(error, PermissionError)

class TestCopyFolder(unittest.TestCase):
    def setUp(self):
        self.source_dir_obj = tempfile.TemporaryDirectory()
        self.source_dir = self.source_dir_obj.name
        self.source_name = 'source_folder'
        self.destination_name = 'target_folder'
        self.source_path = os.path.join(self.source_dir, self.source_name )
        self.destination_path = os.path.join(self.source_dir, self.destination_name )
        self.path_source = Path(self.source_path)
        self.path_destination = Path(self.destination_path)
        
    def tearDown(self):
        self.source_dir_obj.cleanup()
    
    def test_copy_folder_badinput(self):
        test_file = 'testfile'
        self.path_source.mkdir()
        src_file = self.path_source / test_file
        src_content = random_textfile(src_file)
        
        logger_movefsl = logging.getLogger('move_fsl')
        with patch.object(logger_movefsl, 'error', autospec=True) as mock_logger:
            error = move_fsl.move_folder(src_file, self.path_destination, self.path_source, self.path_destination)
        mock_logger.assert_called_once_with("Attempt to call copy_folder on a non-folder object {0}".format(str(src_file)))
        self.assertIsInstance(error, TypeError)
    
    def test_copy_folder(self):
        test_dir = 'testdir' 
        dst_dir = self.path_destination / test_dir
        source_dir = self.path_source / test_dir
        self.path_source.mkdir()
        source_dir.mkdir()
        
        with patch.object(move_fsl.Path, 'mkdir', autospec=True) as mock_mkdir:
            mock_mkdir.return_value = None
            with patch.object(move_fsl, 'process_directory', autospec=True) as mock_procdir:
                move_fsl.copy_folder(source_dir, dst_dir, self.path_source, self.path_destination)
            mock_procdir.assert_called_once_with(source_dir, dst_dir, self.path_source, self.path_destination, verbose=False)
            
            mock_mkdir.assert_called_once_with(dst_dir)
            mock_mkdir.reset_mock()
            mock_mkdir.side_effect = PermissionError("Permission Denied")
            
            with patch.object(move_fsl, 'process_directory', autospec=True) as mock_procdir:
                logger_movefsl = logging.getLogger('move_fsl')
                with patch.object(logger_movefsl, 'error', autospec=True) as mock_logger:
                    error = move_fsl.copy_folder(source_dir, dst_dir, self.path_source, self.path_destination)
                mock_logger.assert_called_once_with("PermissionError: {0} on attempting to create folder {1}".format(str(error), str(dst_dir)))
            self.assertEqual(mock_procdir.call_count, 0)
            mock_mkdir.assert_called_once_with(dst_dir)
        
    def test_move_folder(self):
        test_dir = 'testdir' 
        dst_dir = self.path_destination / test_dir
        source_dir = self.path_source / test_dir
        with patch.object(move_fsl, 'copy_folder', autospec=True) as mock_copy_folder:
            mock_copy_folder.return_value = None
            with patch.object(move_fsl.shutil, 'rmtree', autospec=True) as mock_unlink:
                move_fsl.move_folder(source_dir, dst_dir, self.path_source, self.path_destination)
            mock_unlink.assert_called_once_with(source_dir)
        mock_copy_folder.assert_called_once_with(source_dir, dst_dir, self.path_source, self.path_destination, verbose=False)

    def test_move_folder_fails(self):
        test_dir = 'testdir' 
        dst_dir = self.path_destination / test_dir
        source_dir = self.path_source / test_dir
        with patch.object(move_fsl, 'copy_folder', autospec=True) as mock_copy_folder:
            mock_copy_folder.return_value = Exception('An error')
            with patch.object(move_fsl.shutil, 'rmtree', autospec=True) as mock_unlink:
                error = move_fsl.move_folder(source_dir, dst_dir, self.path_source, self.path_destination)
            self.assertEqual(mock_unlink.call_count, 0)
        mock_copy_folder.assert_called_once_with(source_dir, dst_dir, self.path_source, self.path_destination, verbose=False)
        self.assertIsInstance(error, Exception)

class TestProcessDirectory(unittest.TestCase):
    def setUp(self):
        self.source_dir_obj = tempfile.TemporaryDirectory()
        self.source_dir = self.source_dir_obj.name
        self.source_name = 'source_folder'
        self.destination_name = 'target_folder'
        self.source_path = os.path.join(self.source_dir, self.source_name )
        self.destination_path = os.path.join(self.source_dir, self.destination_name )
        self.path_source = Path(self.source_path)
        self.path_destination = Path(self.destination_path)
        self.path_source.mkdir()
        self.directories = ['adir']
        self.dir_paths = []
        self.files = ['afile']
        self.file_paths = []
        self.links = ['alink']
        self.link_paths = []
        for i,d in enumerate(self.directories):
            self.dir_paths.append(self.path_source/d)
            self.dir_paths[i].mkdir()
        for i,f in enumerate(self.files):
            self.file_paths.append(self.path_source/f)
            random_textfile(self.file_paths[i])
        for i,l in enumerate(self.links):
            self.link_paths.append(self.path_source/l)
            self.link_paths[i].symlink_to('/usr/local')
        
    def tearDown(self):
        self.source_dir_obj.cleanup()

    def test_recursion(self):
        real_processdir = move_fsl.process_directory
        with patch.object(move_fsl, 'process_directory', autospec=True, return_value=None) as mocked_pd:
            with patch.object(move_fsl, 'copy_file', autospec=True, return_value=None) as mocked_cf:
                with patch.object(move_fsl, 'copy_link', autospec=True, return_value=None) as mocked_cl:
                    with patch.object(move_fsl.Path, 'mkdir', autospec=True) as mocked_mkdir:
                        error = real_processdir(self.path_source, self.path_destination, self.path_source, self.path_destination)
        self.assertListEqual(error, [])
        for i,d in enumerate(self.dir_paths):
            mocked_pd.assert_called_with(d, self.path_destination/self.directories[i], self.path_source, self.path_destination, verbose=False)
            mocked_mkdir.assert_called_with(self.path_destination/self.directories[i])
        for i,f in enumerate(self.file_paths):
            mocked_cf.assert_called_with(f, self.path_destination/self.files[i], self.path_source, self.path_destination)
        for i,l in enumerate(self.link_paths):
            mocked_cl.assert_called_with(l, self.path_destination/self.links[i], self.path_source, self.path_destination)
        ndirs = len(self.directories)
        self.assertEqual(mocked_pd.call_count, ndirs)
    
class TestCopy(unittest.TestCase):
    def setUp(self):
        self.source_dir_obj = tempfile.TemporaryDirectory()
        self.source_dir = self.source_dir_obj.name
        self.source_name = 'source_folder'
        self.destination_name = 'target_folder'
        self.source_path = os.path.join(self.source_dir, self.source_name )
        self.destination_path = os.path.join(self.source_dir, self.destination_name )
        self.path_source = Path(self.source_path)
        self.path_destination = Path(self.destination_path)
        self.path_source.mkdir()
        self.path_destination.mkdir()
        
    def tearDown(self):
        self.source_dir_obj.cleanup()
    
    def test_copy_symlink(self):
        slink = 'alink'
        alink = self.path_source / slink
        alink.symlink_to('/usr/local')
        
        error = move_fsl.copy( alink, self.path_destination, validated=True)
        self.assertIsInstance(error, TypeError)
        
    def test_copy_file(self):
        fname = 'afile'
        afile = self.path_source / fname
        random_textfile(afile)
        
        with patch.object(move_fsl, 'copy_file', autospec=True) as cf:
            cf.return_value = None
            errors = move_fsl.copy(afile, self.path_destination)
        
        self.assertIsNone(errors)
        cf.assert_called_once_with(afile, self.path_destination/fname, 
                                   afile, self.path_destination/fname)
    
    def test_move_file(self):
        fname = 'afile'
        afile = self.path_source / fname
        random_textfile(afile)
        
        with patch.object(move_fsl, 'copy_file', autospec=True) as cf:
            with patch.object(move_fsl.Path, 'unlink', autospec=True) as mock_unlink:
                cf.return_value = None
                errors = move_fsl.move(afile, self.path_destination)
        
        self.assertIsNone(errors)
        cf.assert_called_once_with(afile, self.path_destination/fname, 
                                   afile, self.path_destination/fname)
        self.assertEqual(mock_unlink.call_count, 1)
    
    def test_copy_dir(self):
        dname = 'adir'
        adir = self.path_source / dname
        adir.mkdir()
        
        with patch.object(move_fsl, 'process_directory', autospec=True) as pd:
            with patch.object(move_fsl.Path, 'unlink', autospec=True) as mock_unlink:
                pd.return_value = None
                errors = move_fsl.copy(adir, self.path_destination)
        
        self.assertIsNone(errors)
        pd.assert_called_once_with(adir, self.path_destination/dname, 
                                   adir, self.path_destination/dname, verbose=False)
        self.assertEqual(mock_unlink.call_count, 0)
    
    def test_move_dir(self):
        dname = 'adir'
        adir = self.path_source / dname
        adir.mkdir()
        
        with patch.object(move_fsl, 'process_directory', autospec=True) as pd:
            with patch.object(move_fsl.shutil, 'rmtree', autospec=True) as mock_rmtree:
                pd.return_value = None
                errors = move_fsl.move(adir, self.path_destination)
        
        self.assertIsNone(errors)
        pd.assert_called_once_with(adir, self.path_destination/dname, 
                                   self.path_source/adir, self.path_destination/dname, verbose=False)
        self.assertEqual(mock_rmtree.call_count, 1)
        mock_rmtree.assert_called_once_with(str(adir), ignore_errors=False, onerror=move_fsl.error_printer)
    
    def test_copy_create_destination(self):
        afilen = 'afile'
        afile = self.path_source / afilen
        afile_content = random_textfile(afile)
        
        with patch.object(move_fsl, 'copy', autospec=True) as cp:
            cp.return_value = None
            errors = move_fsl.copy(afile, self.path_destination)
        cp.assert_called_once_with(afile, self.path_destination)
        
    def test_move_create_destination(self):
        afilen = 'afile'
        afile = self.path_source / afilen
        afile_content = random_textfile(afile)
        
        with patch.object(move_fsl, 'copy', autospec=True) as cp:
            with patch.object(move_fsl.Path, 'unlink', autospec=True) as mock_unlink:
                cp.return_value = None
                errors = move_fsl.move(afile, self.path_destination)
        cp.assert_called_once_with(afile, self.path_destination, False, False, False)
        self.assertEqual(mock_unlink.call_count, 1)
    
    def test_copy_relative_source_dst(self):
        afilen = 'afile'
        afile = self.path_source / afilen
        afile_content = random_textfile(afile)
        bdir = self.path_source / 'bdir'
        bdir.mkdir()
        os.chdir(self.source_path)
        afile_relpath = Path('afile')
        bdir_relpath = Path('bdir')
        with patch.object(move_fsl, 'copy_file', autospec=True) as cf:
            errors = move_fsl.copy(afile_relpath, bdir_relpath)
        cf.assert_called_once_with(afile_relpath.resolve(), bdir_relpath.resolve()/afilen,
                                   afile_relpath.resolve(), bdir_relpath.resolve()/afilen)

class TestMoveFsl(unittest.TestCase):
    def setUp(self):
        self.source_dir_obj = tempfile.TemporaryDirectory()
        self.source_dir = self.source_dir_obj.name
        self.source_name = 'source_folder'
        self.destination_name = 'target_folder'
        self.source_path = os.path.join(self.source_dir, self.source_name )
        self.destination_path = os.path.join(self.source_dir, self.destination_name )
        self.path_source = Path(self.source_path)
        self.path_destination = Path(self.destination_path)
        
    def tearDown(self):
        self.source_dir_obj.cleanup()

    def test_resolve_destination(self):
        self.assertEqual( self.path_destination,
                          move_fsl.resolve_destination(self.path_source / 'afolder', self.path_destination) )
        self.path_destination.mkdir()
        self.assertEqual( self.path_destination / "afile",
                          move_fsl.resolve_destination(self.path_source / 'afile', self.path_destination) )
        
    def test_move_missing_source(self):
        errors = move_fsl.move(self.source_path, self.destination_path, validated=False)
        self.assertIsInstance(errors, IOError)

    def test_move_missing_destination(self):
        errors = move_fsl.move(self.source_path, '/', validated=False)
        self.assertIsInstance(errors, OSError)
    
    def test_copy_folder_plain(self):
        # source > simple_file
        # send to destination > simple_file creating destination
        #pdb.set_trace()
        os.mkdir(self.source_path)
        os.mkdir(self.destination_path)
        files = ['simple_file', 'simple file with spaces', ]
        
        for fname in files:
            with open(os.path.join(self.source_path, fname), 'w') as f:
                f.write("Some text")
        
        errors = move_fsl.copy(self.source_path, self.destination_path, verbose=False)
        self.assertTrue(os.path.exists(self.source_path))
        self.assertTrue(os.path.exists(self.destination_path))
        for fname in files:
            self.assertTrue(os.path.exists(os.path.join(self.source_path, fname)))
            self.assertTrue((self.path_destination / self.path_source.name / fname).exists())
            self.assertTrue((self.path_destination / self.path_source.name / fname).is_file())
        
    def test_move_plain(self):
        os.mkdir(self.source_path)
        files = ['simple_file', 'simple file with spaces', ]
        
        for fname in files:
            with open(os.path.join(self.source_path, fname), 'w') as f:
                f.write("Some text")
        
        move_fsl.move(self.source_path, self.destination_path, verbose=False)

        self.assertFalse(os.path.exists(self.source_path))
        self.assertTrue(self.destination_path)
        for fname in files:
            self.assertTrue(os.path.exists(os.path.join(self.destination_path, fname)))
            self.assertTrue(os.path.isfile(os.path.join(self.destination_path, fname)))
    
    def test_move_links(self):
        os.mkdir(self.source_path)
        files = ['simple_file', ]
        links = [('alink', './simple_file', True),
                 ('blink', '/usr/local', True),
                 ('clink', '../somewhere', False),
                 ]

        for fname in files:
            with open(os.path.join(self.source_path, fname), 'w') as f:
                f.write("Some text")
        
        for lname in links:
            os.symlink(lname[1], os.path.join(self.source_path, lname[0]))
        move_fsl.move(self.source_path, self.destination_path)
        
        self.assertFalse(os.path.exists(self.source_path))
        self.assertTrue(os.path.exists(self.destination_path))
        for fname in files:
            d_file = os.path.join(self.destination_path, fname)
            self.assertTrue(os.path.exists(d_file))
            self.assertTrue(os.path.isfile(d_file))
        for lname in links:
            if lname[2]:
                self.assertTrue(os.path.exists(os.path.join(self.destination_path, lname[0])))
            else:
                self.assertFalse(os.path.exists(os.path.join(self.destination_path, lname[0])))
            self.assertTrue(os.path.islink(os.path.join(self.destination_path, lname[0])))
            self.assertEqual(os.readlink(os.path.join(self.destination_path, lname[0])), lname[1])
    
    def test_move_feat(self):
        os.mkdir(self.source_path)
        os.mkdir(self.destination_path)
        feat_folder = 'MyExperiment.feat'
        src_feat_dir = os.path.join(self.source_path, feat_folder)
        dest_feat_dir = os.path.join(self.destination_path, feat_folder)
        os.mkdir(src_feat_dir)
        files = ['design.fsf',]
        
        content_template="""
set feat_files(1) "{0}/fmri.nii.gz"
set first_level(1) "{1}/level1.feat"
"""
        for fname in files:
            f_path = os.path.join(src_feat_dir, fname)
            with open(f_path, 'w') as f:
                f.write(content_template.format(f_path, str(self.source_path)))
        move_fsl.move(src_feat_dir, self.path_destination, verbose=False)
        
        self.assertFalse(os.path.exists(src_feat_dir))
        self.assertTrue(os.path.exists(dest_feat_dir))
        for fname in files:
            d_file = os.path.join(dest_feat_dir, fname)
            self.assertTrue(os.path.exists(d_file))
            with open(d_file, 'r') as f:
                line=f.read()
            self.assertEqual(content_template.format(d_file, str(self.source_path)), line)

if __name__ == '__main__':
    hide_warnings = warnings.simplefilter('ignore', UserWarning)
    unittest.main(warnings=hide_warnings)
