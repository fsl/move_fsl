#!/opt/fmrib/conda/python3/envs/system-tools/bin/python

import unittest
import tempfile
import errno
import os
import grp
import stat
import time
import shutil
from pathlib import Path
from unittest.mock import patch, call, Mock
import random, string
import rebase_fsl
import move_fsl
import logging
import subprocess as sp
import pdb
import warnings

class TestRebaseFsl_misc(unittest.TestCase):
    def setUp(self):
        self.temp_dir_obj = tempfile.TemporaryDirectory()
        self.source_dir = self.temp_dir_obj.name
        self.path_source = Path(self.source_dir)

    def tearDown(self):
        self.temp_dir_obj.cleanup()

    def test_gen_backupfname(self):
        tf = self.path_source / 'afile'
        tf_cont = random_textfile(tf)
        backupname = rebase_fsl.gen_backupfname(tf)
        self.assertEqual(backupname, self.path_source / 'afile_bak')
        backup = random_textfile(backupname)
        backupname2 = rebase_fsl.gen_backupfname(tf)
        self.assertEqual(backupname2, self.path_source / 'afile_bak_bak')
        self.assertRaises(PermissionError, rebase_fsl.gen_backupfname, tf, max_recursion=1)
        backupname = rebase_fsl.gen_backupfname(tf, '_backup')
        self.assertEqual(backupname, self.path_source / 'afile_backup')
    
class TestRebaseFsl_parseargs(unittest.TestCase):
    def setUp(self):
        self.temp_dir_obj = tempfile.TemporaryDirectory()
        self.source_dir = self.temp_dir_obj.name
        os.mkdir(os.path.join(self.source_dir, 'source'))
        
    def tearDown(self):
        self.temp_dir_obj.cleanup()
        
    def test_pairs(self):
        options = rebase_fsl.parse_args(['source', 'd1:n1'])
        self.assertEqual(options.item, 'source')
        self.assertEqual(options.path_pairs[0], 'd1:n1')

    def test_multiple_pairs(self):
        options = rebase_fsl.parse_args(['source', 'd1:n1', 'd2:n2', ])
        self.assertEqual(options.item, 'source')
        self.assertListEqual(options.path_pairs, ['d1:n1', 'd2:n2'] )
        
    def test_missing_pairs(self):
        self.assertRaises(SystemExit, rebase_fsl.parse_args, ['source'] )

    def test_backup(self):
        options = rebase_fsl.parse_args(['-b', 'source', 'destination:newdestination'])
        self.assertTrue(options.backup)
        self.assertEqual(options.item, 'source')
        self.assertEqual(options.path_pairs, ['destination:newdestination', ])

    def test_debug(self):
        options = rebase_fsl.parse_args(['-d', 'source', 'destination:newdestination'])
        self.assertTrue(options.debug)

    def test_verbose(self):
        options = rebase_fsl.parse_args(['-v', 'source', 'destination:newdestination'])
        self.assertTrue(options.verbose)

class TestRebaseFsl_validate_item(unittest.TestCase):
    def test_validate_item_badinput(self):
        self.assertRaises(TypeError, rebase_fsl.validate_item, ['a'])
        self.assertRaises(TypeError, rebase_fsl.validate_item, ['a'])
        
    def test_validate_item_symlink(self):
        td = tempfile.TemporaryDirectory()
        tp = Path(td.name)
        tl = tp / 'asymlink'
        tl.symlink_to('/usr/local')
        
        with self.assertRaises(TypeError) as error:
            rebase_fsl.validate_item(tl)
        
        the_exception = error.exception
        if not str(the_exception).endswith("asymlink is not a file or directory"):
            self.fail(str(the_exception))
        td.cleanup()
    
    def test_validate_item_exists(self):
        td = tempfile.TemporaryDirectory()
        tp = Path(td.name)
        tf = tp / 'afile'
        content = random_textfile(tf)
        self.assertTrue(rebase_fsl.validate_item(tf))
        self.assertTrue(rebase_fsl.validate_item(str(tf)))
        td.cleanup()
        
    def test_item_missing(self):
        with self.assertRaises(FileNotFoundError) as oserror:
            rebase_fsl.validate_item(Path('nosource'))
        the_exception = oserror.exception
        self.assertEqual(the_exception.strerror, "nosource: No such file or directory")

    def test_item_folder_permissiondenied(self):
        td = tempfile.TemporaryDirectory()
        os.chmod(td.name, 0)
        self.assertRaises(PermissionError,
                          rebase_fsl.validate_item,
                          Path(td.name))
        os.chmod(td.name, stat.S_IRUSR | stat.S_IWUSR | stat.S_IXUSR)
        td.cleanup()
    
    def test_item_file_permissiondenied(self):
        td = tempfile.NamedTemporaryFile()
        os.chmod(td.name, 0)
        self.assertRaises(PermissionError,
                          rebase_fsl.validate_item,
                          Path(td.name))
        os.chmod(td.name, stat.S_IRUSR | stat.S_IWUSR)
        td.close()
        

class TestRebaseFsl_rebase_file(unittest.TestCase):
    def setUp(self):
        self.td= tempfile.TemporaryDirectory()
        self.tp = Path(self.td.name)
        self.tf = self.tp / 'afile'
    
    def tearDown(self):
        self.td.cleanup()
        
    # Test text replacement
    def test_replace(self):
        orig_text = '/home/fs0/jbloggs/myanalysis.fsf'
        self.tf.write_text(orig_text)
        rebase_fsl.rebase_file(self.tf, [('/home/fs0/loggs', '/vols/Scratch/loggs')])
        content = self.tf.read_text()
        self.assertEqual(content, orig_text)
        rebase_fsl.rebase_file(self.tf, [('/home/fs0/jbloggs', '/vols/Scratch/jbloggs')])
        content = self.tf.read_text()
        self.assertEqual(content, orig_text.replace('/home/fs0/jbloggs', '/vols/Scratch/jbloggs'))
        
    # Test keeping a backup
    def test_replace_backup(self):
        orig_text = '/home/fs0/jbloggs/myanalysis.fsf'
        self.tf.write_text(orig_text)
        rebase_fsl.rebase_file(self.tf, [('/home/fs0/jbloggs', '/vols/Scratch/jbloggs')], keep=True)
        content = self.tf.read_text()
        bf = self.tf.with_name('_'.join((self.tf.name, 'bak')))
        self.assertTrue(bf.exists())
        
        self.assertEqual(content, '/vols/Scratch/jbloggs/myanalysis.fsf')
        bf_content = bf.read_text()
        self.assertEqual(orig_text, bf_content)
    
    def test_not_text(self):
        random_binfile(self.tf)
        rebase_fsllogger = logging.getLogger('rebase_fsl')
        with patch.object(rebase_fsllogger, 'debug', autospec=True) as mock_logger:
            rebase_fsl.rebase_file(self.tf, [])
        mock_logger.assert_called_with("{0} is not a text file, skipping".format(str(self.tf))) 

class TestRebaseFsl_rebase_folder(unittest.TestCase):
    def setUp(self):
        self.td= tempfile.TemporaryDirectory()
        self.tp = Path(self.td.name)
        self.tf = self.tp / 'afile'
    
    def tearDown(self):
        self.td.cleanup()
        
    # Test text replacement
    def test_replace_file(self):
        orig_text = '/home/fs0/jbloggs/myanalysis.fsf'
        self.tf.write_text(orig_text)
        rebase_fsl.rebase_folder(self.tp, [('/home/fs0/loggs', '/vols/Scratch/loggs')])
        content = self.tf.read_text()
        self.assertEqual(content, orig_text)
        rebase_fsl.rebase_folder(self.tp, [('/home/fs0/jbloggs', '/vols/Scratch/jbloggs')])
        content = self.tf.read_text()
        self.assertEqual(content, orig_text.replace('/home/fs0/jbloggs', '/vols/Scratch/jbloggs'))
        
    # Test folder recursion
    def test_replace_folder(self):
        subf = self.tp / 'afolder'
        subf.mkdir()
        orig_rebase_folder = rebase_fsl.rebase_folder
        with patch.object(rebase_fsl, 'rebase_folder', autospec=True) as mock_rebase_folder:
            mock_rebase_folder.return_value = []
            errors = orig_rebase_folder(self.tp, [])
        mock_rebase_folder.assert_called_once_with(subf, [], verbose=False, keep=False)
        print(errors)
    def test_contains_symlink(self):
        self.tf.symlink_to('/usr/local')
        
        rebase_fsllogger = logging.getLogger('rebase_fsl')
        with patch.object(rebase_fsllogger, 'debug', autospec=True) as mock_logger:
            rebase_fsl.rebase_folder(self.tp, [])
        mock_logger.assert_called_with("Skipping symlink {0}".format(str(self.tf))) 

class TestRebaseFsl_validate_pair(unittest.TestCase):
    def test_validate_pair(self):
        self.assertRaises(ValueError, rebase_fsl.validate_pair,'No colon')
        self.assertRaises(ValueError, rebase_fsl.validate_pair, 'A colon:')
        self.assertRaises(ValueError, rebase_fsl.validate_pair, ':A colon')

class TestRebaseFsl_rebase(unittest.TestCase):
    def setUp(self):
        self.td= tempfile.TemporaryDirectory()
        self.tp = Path(self.td.name)
        self.tf = self.tp / 'afile'
    
    def tearDown(self):
        self.td.cleanup()

    def test_string(self):
        random_textfile(self.tf)
        with patch.object(rebase_fsl, 'rebase_file', autospec=True) as mock_rebase_file:
            rebase_fsl.rebase(str(self.tf), ['a:b'])
            mock_rebase_file.assert_called_once_with(self.tf, ['a:b'], keep=False, verbose=False)
    
    def test_notstring(self):
        self.assertRaises(ValueError, rebase_fsl.rebase,0, ['a:b'])
        self.assertRaises(ValueError, rebase_fsl.rebase,0.0, ['a:b'])
        
    def test_nonexistant(self):
        self.assertRaises(FileNotFoundError, rebase_fsl.rebase, self.tf, ['a:b'])

    def test_path_pairs(self):
        random_textfile(self.tf)
        with patch.object(rebase_fsl, 'validate_pair', autospec=True) as mock_validate_pair:
            mock_validate_pair.return_value = None
            with patch.object(rebase_fsl, 'rebase_file', autospec=True) as mock_rebase_file:
                rebase_fsl.rebase(self.tf, ['a:b'])
        mock_validate_pair.assert_called_once_with('a:b')
        self.assertRaises(ValueError, rebase_fsl.rebase, self.tf, 'a:b')

    def test_is_symlink(self):
        self.tf.symlink_to('/usr/local')
        self.assertRaises(TypeError, rebase_fsl.rebase, self.tf, ['a:b'])
        self.assertRaises(TypeError, rebase_fsl.rebase, self.tf, ['a:b'], validated=True)
    
    def test_is_dir(self):
        with patch.object(rebase_fsl, 'rebase_folder', autospec=True) as mock_rebase_folder:
            mock_rebase_folder.return_value = []
            errors = rebase_fsl.rebase(self.tp, [])
        mock_rebase_folder.assert_called_once_with(self.tp, [], verbose=False, keep=False)
        
    def test_is_file(self):
        random_textfile(self.tf)
        with patch.object(rebase_fsl, 'rebase_file', autospec=True) as mock_rebase_file:
            mock_rebase_file.return_value = []
            errors = rebase_fsl.rebase(self.tf, [])
        mock_rebase_file.assert_called_once_with(self.tf, [], verbose=False, keep=False)
        
def random_textfile(pathobj, set_gp=True):
    test_text = ''.join(random.choice(string.ascii_uppercase + string.digits) for _ in range(5))
    pathobj.write_text(test_text)
    # File creation in the temp folder structure sets group to '0'!
    if set_gp:
        shutil.chown(str(pathobj), -1, os.getgroups()[0])
    return test_text

def random_binfile(pathobj, set_gp=True):
    pathobj.write_bytes(os.urandom(1024))
    # File creation in the temp folder structure sets group to '0'!
    if set_gp:
        shutil.chown(str(pathobj), -1, os.getgroups()[0])
    
if __name__ == '__main__':
    hide_warnings = warnings.simplefilter('ignore', UserWarning)
    unittest.main(warnings=hide_warnings)