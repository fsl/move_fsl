.PHONY : install
.PHONY : install-miniconda-linux
.PHONY : install-miniconda-mac

miniconda_root=/opt/fmrib/conda/python3
MC_ACTIVATE=$(miniconda_root)/bin/activate
ENV=system-tools
MC_PYTHON=$(server_miniconda_root)/bin/python
ENV_PYTHON=$(server_miniconda_root)/envs/$(SRV_ENV)/bin/python

UNAME_S := $(shell uname -s)
ifeq ($(UNAME_S),Linux)
    BIN_GROUP=root
endif
ifeq ($(UNAME_S),Darwin)
    BIN_GROUP=wheel
endif

BASE=/opt/fmrib
LIB_FOLDER=$(BASE)/move_fsl
BIN_ROOT=$(BASE)/bin
BIN_OWNER=root
BINS := move_fsl.py rebase_fsl.py

install-miniconda-linux:
ifeq "$(wildcard $(miniconda_root))" ""
		curl -O https://repo.continuum.io/miniconda/Miniconda3-latest-Linux-x86_64.sh
		mkdir -p ${miniconda_root}
		sh Miniconda3-latest-Linux-x86_64.sh -b -f -p ${miniconda_root}
		$(miniconda_root)/bin/conda update -y --all; $(miniconda_root)/bin/conda create -y --name='system-tools' python
		rm Miniconda3-latest-Linux-x86_64.sh
else
		echo "Miniconda already exists"
endif
	
install-miniconda-mac:
ifeq "$(wildcard $(miniconda_root))" ""
		curl -O https://repo.continuum.io/miniconda/Miniconda3-latest-MacOSX-x86_64.sh
		mkdir -p ${miniconda_root}
		sh Miniconda3-latest-MacOSX-x86_64.sh -b -f -p ${miniconda_root}
		$(miniconda_root)/bin/conda update -y --all; $(miniconda_root)/bin/conda create -y --name='system-tools' python
		rm Miniconda3-latest-MacOSX-x86_64.sh
else
		echo "Miniconda already exists"
endif

install: move_fsl.py rebase_fsl.py
	mkdir -p $(LIB_FOLDER)
	for bin in $(BINS); do \
		cp $${bin} /tmp/$${bin}-preinstall; \
		base=`basename $${bin} .py`; \
		sed -i -e 's|#!/usr/bin/python|#!${ENV_PYTHON}|' /tmp/$${bin}-preinstall; \
		install -p -o ${BIN_OWNER} -g ${BIN_GROUP} -m 555 /tmp/$${bin}-preinstall ${LIB_FOLDER}/$${bin}; \
		(source ${MC_ACTIVATE} system-tools && python -m compileall ${LIB_FOLDER}/$${bin}); \
		rm /tmp/$${bin}-preinstall; \
		rm -f ${BIN_ROOT}/$${base}; \
		ln -s ${LIB_FOLDER}/$${bin} ${BIN_ROOT}/$${base}; \
	done