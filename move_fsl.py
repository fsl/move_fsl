#!/opt/fmrib/conda/python3/envs/system-tools/bin/python
# Script to automagically move a file/folder to a different file system re-writing/linking 
# from the source to the destination folder.

import argparse
import logging
import os
import sys
import errno
import shutil
import pwd, grp
from pathlib import Path, PosixPath, WindowsPath
import warnings
from functools import lru_cache
from sys import stderr
try:
    import magic
    NO_MAGIC=False
except ImportError:
    import subprocess as sp
    NO_MAGIC=True

FILE=0
DIRECTORY=1
SYMLINK=2

warnings.simplefilter('always', UserWarning)

# File extensions to skip path munging
SKIP_EXTS = set(['png', 'nii', 'gz', 'ppm', 'gif', 'jpg', 'zip', 'vol', 'msf', 'rdata', ])
# File names to skip path munging
SKIP_FILES = set([ 'design.con', 'design.mat', 'design.trg', 'design.frf', 'absbrainthresh.txt', 'bvals', 'bvecs',
                  'acqparams.txt', ])
# File names to force path munging
FIX_FILES = set([ 'design.fsf', 'targets.txt', ])
# File extenstions to force path munging
FIX_EXTS = set([ 'html', ])
# Errors occuring during rmtree execution
RMTREE_ERRORS=[]

def remove_alias(alias):
    global FS_ALIASES
    try:
        FS_ALIASES.remove(alias)
    except KeyError:
        warnings.warn("{0} not in list of aliases".format(alias))

def add_alias(alias):
    global FS_ALIASES
    if alias in FS_ALIASES:
        warnings.warn("{0} already in list of skipped extensions".format(alias))
    else:
        FS_ALIASES.add(alias)

# Create aliases for disc location
FS_ALIASES = set()
add_alias(('/vols/Scratch/%user%', '/home/fs0/%user%/scratch'))

def get_username():
    return pwd.getpwuid( os.geteuid() )[0]

def _macro_replace(text, macro, subst):
    return text.replace(macro, subst)

def user_macro(text, username=None):
    return _macro_replace(text, "%user%", username if username else get_username())

def add_skip_ext(extensions):
    '''Add an extension(s) to the skip set'''
    if isinstance(extensions, str):
        extensions = [extensions]
    elif not isinstance(extensions, list) and not isinstance(extensions, tuple):
        raise TypeError("Takes a list/tuple of strings or a string")
    global SKIP_EXTS
    for ext in extensions:
        if not isinstance(ext, str):
            raise TypeError("Extension(s) must be strings")
        if ext.startswith('.'):
            ext = ext.lstrip('.')
        ext = ext.lower()
        if ext in SKIP_EXTS:
            warnings.warn("{0} already in list of skipped extensions".format(ext))
        else:
            SKIP_EXTS.add(ext)

def remove_skip_ext(extensions):
    '''Remove an extension from the skip set'''
    if isinstance(extensions, str):
        extensions = [extensions]
    elif not isinstance(extensions, list) and not isinstance(extensions, tuple):
        raise TypeError("Takes a list/tuple of strings or a string")
    global SKIP_EXTS
    for ext in extensions:
        if not isinstance(ext, str):
            raise TypeError("Extensions must be strings")
        if ext.startswith('.'):
            ext = ext.lstrip('.')
        ext = ext.lower()
        try:
            SKIP_EXTS.remove(ext)
        except KeyError:
            warnings.warn("{0} not in list of skipped extensions".format(ext))

def add_skip_file(file_names):
    '''Add a file name to the skip set'''
    if isinstance(file_names, str):
        file_names = [file_names]
    elif not isinstance(file_names, list) and not isinstance(file_names, tuple):
        raise TypeError("Takes a list/tuple of strings or a string")
    global SKIP_FILES
    for f in file_names:
        if not isinstance(f, str):
            raise TypeError("File names must be strings")
        f = f.lower()
        if f in SKIP_FILES:
            warnings.warn("{0} already in list of skipped files".format(f))
        else:
            SKIP_FILES.add(f)

def remove_skip_file(file_names):
    '''Remove a file name from the skip set'''
    if isinstance(file_names, str):
        file_names = [file_names]
    elif not isinstance(file_names, list) and not isinstance(file_names, tuple):
        raise TypeError("Takes a list/tuple of strings or a string")
    global SKIP_FILES
    for f in file_names:
        if not isinstance(f, str):
            raise TypeError("File names must be strings")
        f = f.lower()
        try:
            SKIP_FILES.remove(f)
        except KeyError:
            warnings.warn("{0} not in list of skipped files".format(f))

def add_fix_ext(extensions):
    '''Add an extension to the fix set'''
    if isinstance(extensions, str):
        extensions = [extensions]
    elif not isinstance(extensions, list) and not isinstance(extensions, tuple):
        raise TypeError("Takes a list/tuple of strings or a string")
    global FIX_EXTS
    for ext in extensions:
        if ext.startswith('.'): ext = ext.lstrip('.')
        ext = ext.lower()
        if ext in FIX_EXTS:
            warnings.warn("{0} already in list of auto-fixed extensions".format(ext))
        else:
            FIX_EXTS.add(ext)

def remove_fix_ext(extensions):
    '''Remove an extension from the fix set'''
    if isinstance(extensions, str):
        extensions = [extensions]
    elif not isinstance(extensions, list) and not isinstance(extensions, tuple):
        raise TypeError("Takes a list/tuple of strings or a string")
    global FIX_EXTS
    for ext in extensions:
        if ext.startswith('.'): ext = ext.lstrip('.')
        if not isinstance(ext, str):
            raise TypeError("Extensions must be strings")
        ext = ext.lower()
        try:
            FIX_EXTS.remove(ext)
        except KeyError:
            warnings.warn("{0} not in list of auto-fixed extensions".format(ext))

def add_fix_file(file_names):
    '''Add a file name to the fix set'''
    if isinstance(file_names, str):
        file_names = [file_names]
    elif not isinstance(file_names, list) and not isinstance(file_names, tuple):
        raise TypeError("Takes a list/tuple of strings or a string")
    global FIX_FILES
    for f in file_names:
        if not isinstance(f, str):
            raise TypeError("File names must be strings")
        f = f.lower()
        if f in FIX_FILES:
            warnings.warn("{0} already in list of auto-fixed file names".format(f))
        else:
            FIX_FILES.add(f)

def remove_fix_file(file_names):
    '''Remove a file name from the fix set'''
    if isinstance(file_names, str):
        file_names = [file_names]
    elif not isinstance(file_names, list) and not isinstance(file_names, tuple):
        raise TypeError("Takes a list/tuple of strings or a string")
    global FIX_FILES
    for f in file_names:
        if not isinstance(f, str):
            raise TypeError("File names must be strings")
        f = f.lower()
        try:
            FIX_FILES.remove(f)
        except KeyError:
            warnings.warn("{0} already in list of auto-fixed file names".format(f))
    
def parse_args(args):
    parser = argparse.ArgumentParser()
    parser.add_argument("-c", "--copy", help="Copy (rather than move) the file/folder.",
                        action="store_true")
    parser.add_argument("-v", "--verbose", help="Print progress.",
                        action="store_true")
    parser.add_argument("-d", "--debug", help="Debug mode",
                        action="store_true")
    parser.add_argument("-f", "--force", help="Force overwrite of existing files/folders",
                        action="store_true")
    parser.add_argument('source', help="Source file/folder(s)",
                        nargs='+')
    parser.add_argument('destination', help="Destination file/folder.",
                        nargs='?')
    # The destination argument will never be set because we wish to support multiple source folders,
    # this 'fake' argument is there to ensure the help text is correct.
    
    arguments = parser.parse_args(args)

    if len(arguments.source) < 2:
        parser.error("No source/destination specified")
    
    arguments.destination = arguments.source.pop()
    
    return arguments

def writeable(path):
    return os.access(str(path), os.W_OK)
def readable(path):
    return os.access(str(path), os.R_OK)
def readabledir(path):
    return os.access(str(path), os.R_OK | os.X_OK )

def ask_user_confirm(question):
    user_input = ''
    while user_input not in [ 'y', 'Y', 'n', 'N', 'Yes', 'No']:
        user_input = input(question)
    return user_input in [ 'y', 'Y', 'Yes' ]

class NotAFileOrDirectoryError(OSError):
    pass

def validate_input(sources, target, force=False):
    '''Confirm that sources can be written to target'''
    logger = logging.getLogger(__name__)
    logger.debug("Checking s: {0}; d: {1}; f: {2}".format(sources,target,force))
    if isinstance(sources, list):
        if len(sources) == 1:
            single = False
        else:
            single = True
    else:
        single = True
        sources = [sources]
    
    if not any([ isinstance(s, Path) for s in sources]) or not isinstance(target, Path):
        raise TypeError("validate_input takes Pathlib objects")
    def overwrite(path):
        return ask_user_confirm("{} exists, do you want to overwrite? [y|n]:".format(path))
    
    def check_target(target, source, force):
        logger = logging.getLogger(__name__)
        if target.is_dir():
            logger.debug("{} is a directory".format(target))
            if writeable(target):
                logger.debug("{} is a writeable".format(target))
                target_source = target / source
                if (target_source).exists():
                    logger.debug("{} exists".format(target_source))
                    if not writeable(target_source):
                        raise PermissionError(str(target_source))
                    if not force and not overwrite(target_source):
                        raise FileExistsError(str(target_source))
            else:
                raise PermissionError(str(target/source))
        else:
            if not force and not overwrite(target):
                raise FileExistsError(str(target))
  
    for s in sources:
        logger.debug("Checking source {}".format(s))
        if s.exists():
            if s.is_symlink() or not s.is_file() and not s.is_dir():
                raise NotAFileOrDirectoryError(str(s))
            if ( s.is_dir() and not readabledir(s) ) or not readable(s):
                raise PermissionError(str(target))
            if target.exists():
                logger.debug("Target {} exists - checking it's contents".format(target))
                if not single and not target.is_dir():
                    raise NotADirectoryError(str(target))
                logger.debug("Checking for {0}/{1}".format(target, s.name))
                check_target(target, s.name, force)
            else:
                if single:
                    if target.parent.exists():
                        if not target.parent.is_dir():
                            raise NotADirectoryError(str(target))
                        if not writeable(target.parent):
                            raise PermissionError(str(target.parent))
                    else:
                        raise FileNotFoundError(str(target.parent))
                else:
                    raise FileNotFoundError(str(target))
        else:
            raise FileNotFoundError(s)

def ftype(fobject):
    '''Return file, directory or symlink depending on what the Path object fobject is'''
    if fobject.is_symlink():
        return SYMLINK
    elif fobject.is_file():
        return FILE
    elif fobject.is_dir():
        return DIRECTORY
    else:
        raise TypeError("Unexpected filesystem object type")

def copy(source, destination, validated=False, verbose=False, force=False):
    '''Copy a file or folder structure, continuing on errors. Returns None for success or a nested list of errors experienced'''
    logger = logging.getLogger(__name__)
    if isinstance(source, str):
        source = Path(source)
    if isinstance(destination, str):
        destination = Path(destination)
    has_errors = None
    logger.debug("Requested copying of {0} to {1} fixing paths/links as necessary".format(source,destination))
    if not validated:
        try:
            validate_input(source, destination, force)
            source_type = ftype(source)
        except Exception as e:
            return e
    else:
        try:
            source_type = ftype(source)
        except Exception as e:
            return e
    # Validation should only let through a source of file or directory
    # Record the starting points:
    
    if not source.is_absolute():
        source = source.resolve()
    
    if not destination.is_absolute():
        if destination.exists():
            destination = destination.resolve()
        else:
            destination = Path.cwd() / destination

    if destination.is_dir():
        dst = destination / source.name
    else:
        dst = destination
    
    if verbose: print(str(source))
    if source_type == FILE:
        has_errors = copy_file(source, dst, source, dst)
    elif source_type == DIRECTORY:
        has_errors = copy_folder(source, dst, source, dst, verbose=verbose)
    else:
        return TypeError("Unsupported object type {0} not copied".format(str(source)))
    
    return has_errors

def move(source, destination, validated=False, verbose=False, force=False):
    logger = logging.getLogger(__name__)
    logger.debug("Requested moving of {0} to {1} fixing paths/links as necessary".format(source,destination))
    if isinstance(source, str):
        source = Path(source)
    if isinstance(destination, str):
        destination = Path(destination)
    has_errors = None
    
    has_errors = copy(source, destination, validated, verbose, force)
    if not has_errors:
        if source.is_file():
            try:
                logger.debug("Removing {0}".format(str(source)))
                source.unlink()
            except PermissionError as e:
                has_errors = e
                logger.error("Permission denied when deleting {0}".format(str(source)))
        elif source.is_dir():
            logger.debug("Removing {0}".format(str(source)))
            shutil.rmtree(str(source), ignore_errors=False, onerror=error_printer)
            if RMTREE_ERRORS:
                has_errors.append(RMTREE_ERRORS)
    return has_errors

def error_printer(function, path, excinfo):
    logger = logging.getLogger(__name__)
    if function == os.path.islink():
        logger.error("Error removing link {0} ({1})".format(path, str(excinfo[1])))
    elif function == os.listdir():
        logger.error("Error enumerating directory {0} ({1})".format(path, str(excinfo[1])))
    elif function == os.remove():
        logger.error("Error removing file {0} ({1})".format(path, str(excinfo[1])))
    elif function == os.rmdir():
        logger.error("Error removing directory {0} ({1})".format(path, str(excinfo[1])))
    RMTREE_ERRORS.append(excinfo)
    
def process_directory(source, destination, src_parent, dst_parent, verbose=False):
    logger = logging.getLogger(__name__)
    logger.debug("Processing directory" + str(source))
    errors = []
    
    for child in source.iterdir():
        error = None
        if verbose: print(str(child))
        # Always check symlinks first as is_dir and is_file will follow symlinks!
        if child.is_symlink():
            logger.debug("Moving symlink {0} to {1}".format(str(child), str(destination)))
            error = copy_link(child, destination / child.name, src_parent, dst_parent)
        elif child.is_dir():
            logger.debug("Moving folder {0} to {1}".format(str(child), str(destination)))
            error = copy_folder(child, destination / child.name, src_parent, dst_parent, verbose=verbose)
        elif child.is_file():
            logger.debug("Moving file {0} to {1}".format(str(child), str(destination)))
            error = copy_file(child, destination / child.name, src_parent, dst_parent)
        else:
            logger.debug("Unsupported object type {0}".format(str(child)))
            logger.error("Unsupported object type {0} not copied".format(str(child)))
        if error:
            errors.append((str(child), error))
    return errors

@lru_cache(maxsize=128, typed=False)
def resolve_destination(source, destination):
    '''If destination exists and is a folder, return destination/source, else return destination.'''
    if destination.exists() and destination.is_dir():
        return destination / source.name
    else:
        return destination
    
def copy_link(src_link, dst_link, src_parent, dst_parent):
    '''Copy a symlink from source to destination (both are pathlib objects)'''
    logger = logging.getLogger(__name__)
    
    logger.debug("Requested copy of symlink {0} to {1} fixing as necessary".format(str(src_link),
                                                                                   str(dst_link)))
    target = os.readlink(str(src_link))
    # Warning on OS X - if we are working in /tmp then relative symlinks break!
    # /tmp/alink -> ../usr/local
    # os.readlink('/tmp/alink') -> ../usr/local
    # source.resolve() -> FileNotFoundError due to /private/usr not existing
    # (/tmp is a symlink to /private/tmp)
    if target.startswith(str(src_parent)):
        logger.debug("Re-writing {0} to {1}".format(src_parent, dst_parent))
        target = target.replace(str(src_parent), str(dst_parent), 1)
    error = None
    try:
        dst_link.symlink_to(target)
    except FileExistsError as e:
        logger.error("FileExistsError: {0} on creating link {1}".format(str(e), str(dst_link)))
        error = e
    except PermissionError as e:
        logger.error("PermissionError: {0} on creating link {1}".format(str(e), str(dst_link)))
        error = e
    return error

def move_link(src_link, dst_link, src_parent, dst_parent):
    '''Move a symlink from source to destination (both are pathlib objects)'''
    logger = logging.getLogger(__name__)
    
    logger.debug("Requested move of symlink {0} to {1} fixing as necessary".format(str(src_link),
                                                                                   str(dst_link)))
    error = copy_link(src_link, dst_link, src_parent, dst_parent)
    if error == None:
        logger.debug("Removing source link {0}".format(str(src_link)))
        try:
            src_link.unlink()
        except PermissionError as e:
            logger.error("PermissionError: {0} on removing link {1}".format(str(e), str(dst_link)))
            error = e
    return error

@lru_cache(maxsize=128, typed=False)
def group_membership(username=None):
    if username:
        pwd_entry = pwd.getpwnam(username)
        user_groups = [grp.getgrgid(g).gr_name for g in os.getgrouplist(username, pwd_entry.pw_gid)]
    else:
        user_groups = [grp.getgrgid(g).gr_name for g in os.getgroups()]
    return user_groups

def _copy_file(src, dst):
    logger = logging.getLogger(__name__)
    error = None
    try:
        shutil.copy2(str(src), str(dst), follow_symlinks=False)
        # Attempt to fix the group
        try:
            clone_fsobject(src, dst, group_only=True)
        except PermissionError as e:
            logger.error("PermissionError: {0} - group of file {1} not changed".format(str(e), str(dst)))
            error = e
    except OSError as e:
        logger.error("OSError: {0} on copying file {1}".format(str(e), str(src)))
        error = e
    return error

def clone_fsobject(src, dst, group_only=False):
    '''Clone group and permissions from src to dst'''
    logger = logging.getLogger(__name__)
    if not group_only:
        stats = src.stat()
        if dst.is_symlink():
            pass
        elif dst.is_file():
            os.utime(str(dst), (stats.st_atime, stats.st_mtime))
        elif dst.is_dir():
            dst.chmod(stats.st_mode)
        else:
            raise TypeError("Unsupported filesystem object type")
        
    src_group = src.group()
    dst_group = dst.group()
    if dst_group != src_group:
        if src_group in group_membership():
            shutil.chown(str(dst), -1, src_group)
        else:
            logger.warning("Not setting group on {0} as not a member of group {1}".format(dst, src_group))

def copy_file(source, destination, src_parent, dst_parent):
    new_file = resolve_destination(source, destination)
    logger = logging.getLogger(__name__)
    error = None
    if file_isempty(source):
        logging.debug("File {0} is empty, just copy it".format(str(source)))
        error = _copy_file(source, destination)
        return error
    
    suffix = source.suffix.lstrip('.')
    if suffix in FIX_EXTS or source.name in FIX_FILES:
        logging.debug("Re-writing paths in {0}".format(str(source)))
        error = clean_file(source, destination, src_parent, dst_parent)
    elif suffix in SKIP_EXTS or source.name in SKIP_FILES:
        # Copy the file without checking content
        logging.debug("Copying {0} unchanged".format(str(source)))
        error = _copy_file(source, destination)
    else:
        try:
            if file_istext(source):
                logging.debug("Re-writing paths in {0}".format(str(source)))
                error = clean_file(source, destination, src_parent, dst_parent)
            else:
                error = _copy_file(source, destination)
        except OSError as e:
            logging.error("{0}: {1}".format(str(source), str(e)))
            error = e
    return error

def expand_aliases():
    if ( os.geteuid == 0 ):
        # Running as root, who owns src?
        try:
            username = path.owner()
        except:
            username = None
    else:
        username = get_username()
    aliases = []
    for a in FS_ALIASES:
        if username and "%user%" in a[0]:
            real = user_macro(a[0], username)
            alias = user_macro(a[1], username)
        else:
            real = a[0]
            alias = a[1]
        aliases.append( (real, alias) )
    return aliases

def copy_file_rewrite(src, dst, searches):
    '''Copy a file from src to dst replacing content based on the searches tuple of (search, replace)'''
    logger = logging.getLogger(__name__)
    aliases = expand_aliases()
    encoding = get_fileencoding(src)
    try:
        with src.open(encoding=encoding, newline='') as s:
            with dst.open(mode='w', encoding=encoding, newline='') as d:
                for l in s.readlines():
                    logger.debug("Line before:{0}".format(l))
                    for a in aliases:
                        aliased_l = l.replace(a[1], a[0])
                    for match,replace in searches:
                        new_l = aliased_l.replace(match, replace)
                    d.write(new_l)
                    logger.debug("Line after:{0}".format(new_l))
    except Exception as e:
        logging.error("{0}: {1}".format(str(src), str(e)))
        if dst.exists():
            logging.error("Cleaning up destination file {0}".format(str(src)))
            dst.unlink()
        raise e
    try:
        clone_fsobject(src, dst)
    except PermissionError as e:
        logger.error("PermissionError: {0} - metadata of file {1} not changed".format(str(e), str(dst)))
        raise PermissionError("{0} - metadata of file {1} not changed".format(str(e), str(dst)))
    
def clean_file(src, dst, src_parent, dst_parent):
    logger = logging.getLogger(__name__)
    try:
        copy_file_rewrite(src, dst, (( str(src_parent), str(dst_parent) ),))
    except Exception as e:
        return e

def get_fileencoding(filename):
    '''Get encoding of path object filename'''
    # Linux can use filemagic
    # conda install -c https://conda.anaconda.org/auto filemagic
    # or (until Py3.5 support is added)
    # pip install -i https://pypi.anaconda.org/pypi/simple filemagic
    if isinstance(filename, str):
        filename = Path(filename)
    if not isinstance(filename, PosixPath):
        raise TypeError("file_istext takes a PosixPath object or string")
    if not filename.exists():
        raise OSError(errno.ENOENT, '{0} no such file or directory'.format(str(filename)))
    if NO_MAGIC:
        cmd_out = sp.run(['/usr/bin/file', '-b', '--mime-encoding', str(filename)],
                         stdout=sp.PIPE, stderr=sp.PIPE, universal_newlines=True)
        mime_encoding = cmd_out.stdout
        if mime_encoding.startswith('cannot open'):
            raise OSError(errno.ENOENT, filename)
    else:
        with magic.Magic(flags=magic.MAGIC_MIME_ENCODING) as m:
            mime_encoding = m.id_filename(str(filename))
    return mime_encoding.strip()
    
def file_isempty(filename):
    '''Is path object filename empty?'''
    try:
        return filename.stat().st_size == 0
    except OSError:
        return False
    
def file_istext(filename):
    '''Is path object filename text?'''
    # Linux can use filemagic
    # conda install -c https://conda.anaconda.org/auto filemagic
    # or (until Py3.5 support is added)
    # pip install -i https://pypi.anaconda.org/pypi/simple filemagic
    if isinstance(filename, str):
        filename = Path(filename)
    if not isinstance(filename, PosixPath):
        raise TypeError("file_istext takes a PosixPath object or string")
    if not filename.exists():
        raise OSError(errno.ENOENT, '{0} no such file or directory'.format(str(filename)))
    if NO_MAGIC:
        cmd_out = sp.run(['/usr/bin/file', '-b', '--mime-type', str(filename)],
                         stdout=sp.PIPE, stderr=sp.PIPE, universal_newlines=True)
        mime_type = cmd_out.stdout
        if mime_type.startswith('cannot open'):
            raise OSError(errno.ENOENT, filename)
    else:
        with magic.Magic(flags=magic.MAGIC_MIME_TYPE) as m:
            mime_type = m.id_filename(str(filename))
    if mime_type.startswith('text/'):
        return True
    return False
    
def move_file(src, dst, src_parent, dst_parent):
    '''Move a file from source to destination (both are pathlib objects)'''
    logger = logging.getLogger(__name__)
    logger.debug("Requested move of file {0} to {1} fixing content as necessary".format(str(src),
                                                                                        str(dst)))
    error = copy_file(src, dst, src_parent, dst_parent)
    if not error:
        logger.debug("Removing source file {0}".format(str(src)))
        src.unlink()
    return error

def copy_folder(src_dir, new_dir, src_parent, dst_parent, verbose=False):
    '''Copy a folder from src_dir to new_dir (both are pathlib objects)'''
    logger = logging.getLogger(__name__)
    error = None
    logger.debug("Requested copying of folder {0} to {1} fixing paths/links as necessary".format(str(src_dir),
                                                                                         str(new_dir)))
    if src_dir.is_dir():
        try:
            # Create new folder
            new_dir.mkdir()
        except FileExistsError as e:
            if new_dir.is_dir():
                pass
            else:
                logger.error("FileExistsError: {0} on attempting to create folder {1}".format(str(e), str(new_dir)))
        except PermissionError as e:
            logger.error("PermissionError: {0} on attempting to create folder {1}".format(str(e),
                                                                                          str(new_dir)))
            error = e
        else:
            # -------------------------------------------------
            # Remember to preserve group and permissions
            error = process_directory(src_dir, new_dir, src_parent, dst_parent, verbose=verbose)
    else:
        logger.error("Attempt to call copy_folder on a non-folder object {0}".format(str(src_dir)))
        error = TypeError("copy_folder: Unsupported object type {0} not copied".format(str(src_dir)))

    return error

def move_folder(src_dir, new_dir, src_parent, dst_parent, verbose=False):
    '''Move a folder from src_dir to new_dir (both are pathlib objects)'''
    logger = logging.getLogger(__name__)
    error = None
    logger.debug("Requested move of directory {0} to {1} fixing content as necessary".format(str(src_dir),
                                                                                             str(new_dir)))
    error = copy_folder(src_dir, new_dir, src_parent, dst_parent, verbose=verbose)
    if not error:
        logger.debug("Removing source file {0}".format(str(src_dir)))
        try:
            shutil.rmtree(src_dir)
        except PermissionError as e:
            error = e
            logger.error("PermissionError {0} when attempting to remove folder {1}".format(str(e),
                                                                                           str(src_dir)))
    return error

def die(message):
    logger = logging.getLogger(__name__)
    logger.error(message)
    sys.exit(1)
    
def main():
    try:
        options = parse_args(sys.argv[1:])
        has_errors = []
        logger = logging.getLogger(__name__)
        eh = logging.StreamHandler()
        eh.setLevel(logging.ERROR)
        ef = logging.Formatter('%(message)s')
        eh.setFormatter(ef)
        logger.addHandler(eh)
        if options.debug:
            logger.setLevel(logging.DEBUG)
            eh.setLevel(logging.DEBUG)
        try:
            destination_path = Path(options.destination).expanduser()
            source_paths = [ Path(i).expanduser() for i in options.source ]
            validate_input(source_paths, destination_path, options.force)
        except FileNotFoundError as e:
            die("File/directory {0} not found".format(e))
        except NotADirectoryError as e:
            die("Item {0} is not a directory".format(e))
        except PermissionError as e:
            die("Permissions on {0} do not allow this".format(e))
        except NotAFileOrDirectoryError as e:
            die("Item {0} is not a file or directory".format(e))
        except FileExistsError as e:
            die("Item {0} already exists".format(e))
        except TypeError as e:
            die("Internal error - incorrect object type passed to validator ({0})".format(e))
        for source_path in source_paths:
            if options.copy:
                method = copy
            else:
                method = move
            source_errored = method(source_path, destination_path, validated=True, verbose=options.verbose, force=options.force)
            if source_errored:
                has_errors.append(source_errored)
        if has_errors:
            logger.debug(has_errors)
            sys.exit(1)
    except Exception as e:
        print(e, file=stderr)

if __name__ == '__main__':
    main()