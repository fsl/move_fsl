#!/opt/fmrib/conda/python3/envs/system-tools/bin/python
# Script to fix paths within FSL folders
import argparse
import logging
import os
import sys
import errno
import shutil
import pwd, grp
from pathlib import Path, PosixPath, WindowsPath
import warnings

import move_fsl
# Use move_fsl tools to fix up a folder structure

def rebase(item, path_pairs, verbose=False, validated=False, keep=False):
    '''Process item replacing search with replace from each colon separated path_pair.'''
    logger = logging.getLogger(__name__)
    errors = []
    
    if not isinstance(item, Path):
        if isinstance(item, str):
            item = Path(item)
        else:
            raise ValueError('{0} must be a string or pathlib.Path'.format(str(item)))
    if not validated:
        validate_item(item)
        if not isinstance(path_pairs, list):
            raise ValueError('{0} must be a list of colon separated search:replace pairs'.format(str(path_pairs)))
        for p in path_pairs:
            validate_pair(p)
    
    if verbose: print(item)
    if item.is_symlink():
        raise TypeError('{0} must be a file or folder'.format(str(item)))
    if item.is_file():
        try:
            rebase_file(item, path_pairs, verbose=verbose, keep=keep)
        except Exception as e:
            errors = [e]
    elif item.is_dir():
        logger.debug("{0} is a folder, looking inside...".format(str(item)))
        errors = rebase_folder(item, path_pairs, verbose=verbose, keep=keep)
    else:
        raise TypeError('{0} must be a file or folder'.format(str(item)))
    return errors

def gen_backupfname(basefile, extension="_bak", max_recursion=5):
    target = Path(str(basefile) + extension)
    recursion=1
    while target.exists():
        target = Path(str(target) + extension)
        recursion+=1
        if max_recursion < recursion:
            raise PermissionError("Maximum number of backups created, please clean up old backups of {0}".format(str(basefile)))
    return target

def rebase_file(item, path_pairs, verbose=False, keep=False):
    logger = logging.getLogger(__name__)
    logger.debug("Looking in file {0} for matches to replace".format(str(item)))
    # Copy to a file, fixing in the process
    target = gen_backupfname(item, extension='_tmp')
    if move_fsl.file_istext(item):
        move_fsl.copy_file_rewrite(item, target, path_pairs)
        if not keep:
            target.replace(item)
        else:
            backup = gen_backupfname(item, extension='_bak')
            item.replace(backup)
            target.replace(item)
    else:
        logger.debug("{0} is not a text file, skipping".format(str(item)))
    
def rebase_folder(item, path_pairs, verbose=False, keep=False):
    logger = logging.getLogger(__name__)
    logger.debug("Processing directory" + str(item))
    errors = []
    
    for child in item.iterdir():
        error = None
        if verbose: print(str(child))
        # Always check symlinks first as is_dir and is_file will follow symlinks!
        if child.is_symlink():
            logger.debug("Skipping symlink {0}".format(str(child)))
            pass
        elif child.is_dir():
            error = rebase_folder(child, path_pairs, verbose=verbose, keep=False)
        elif child.is_file():
            try:
                rebase_file(child, path_pairs, verbose=verbose, keep=False)
            except Exception as e:
                error=e
        else:
            logger.debug("Skipping {0} unsupported object type".format(str(child)))
        if error:
            errors.append((str(child), error))
    return errors
    
def parse_args(args):
    parser = argparse.ArgumentParser()
    parser.add_argument("-b", "--backup", help="Preserve backups of changed files (with extension .bak).",
                        action="store_true")
    parser.add_argument("-v", "--verbose", help="Print progress.",
                        action="store_true")
    parser.add_argument("-d", "--debug", help="Debug mode",
                        action="store_true")
    parser.add_argument('item', help="File or folder to process")
    parser.add_argument('path_pairs', help="Colon separated original and changed path[s] (e.g. /home/jbloggs:/scratch/jbloggs)",
                        nargs="+")
    # The destination argument will never be set because we wish to support multiple source folders,
    # this 'fake' argument is there to ensure the help text is correct.
    
    arguments = parser.parse_args(args)
    return arguments

def validate_item(item):
    '''Confirm that item (a pathlib Path or string) exists and is writeable/searchable'''
    if isinstance(item, str):
        item = Path(item)
    if type(item) != Path and type(item) != PosixPath and type(item) != WindowsPath:
        raise TypeError("Takes a pathlib Path object")
    if not item.exists():
        raise FileNotFoundError(errno.ENOENT, "{0}: No such file or directory".format(str(item)))
    if item.is_symlink():
        raise TypeError("{0} is not a file or directory".format(str(item)))
    if not item.is_file() and not item.is_dir():
        raise TypeError("{0} is not a file or directory".format(str(item)))
    if not os.access(str(item), os.W_OK):
        raise PermissionError(errno.EACCES, "{0}: Permission denied".format(str(item)))
    return True

def validate_pair(pair):
    try:
        (orig,dest) = pair.split(':')
    except ValueError:
        raise ValueError("Search/replace pair missing colon separator")
    if not orig or not dest:
        raise ValueError("Search/replace pair missing one half of pair")

def main():
    try:
        options = parse_args(sys.argv[1:])
        has_errors = []
        logger = logging.getLogger(__name__)
        eh = logging.StreamHandler()
        eh.setLevel(logging.ERROR)
        ef = logging.Formatter('%(message)s')
        eh.setFormatter(ef)
        logger.addHandler(eh)
        if options.debug:
            logger.setLevel(logging.DEBUG)
        try:
            validate_item(item)
            for pair in options.path_pairs:
                validate_pairs(pair)
        except (FileNotFoundError, TypeError, ValueError, PermissionError) as e:
            logger.error(e.strerror)
            sys.exit(1)
        errors = rebase(item, path_pairs, validated=True, verbose=options.verbose)
        if errors:
            sys.exit(1)
    except Exception as e:
        print(e, file=stderr)

if __name__ == '__main__':
    main()