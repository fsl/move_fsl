# Move FSL tool

## Purpose
These tools allow the moving of FEAT folders (and some other content) between
locations, correcting paths in the process.

## move_fsl.py
This tool will move (or copy), for example, a FEAT folder ensuring that
references to the original location are updated to the new location. This tool
will **not** correct any references to files outside of the FEAT folder.

## Usage
    move_fsl.py [-h] [-c] [-v] [-d] source [source ...] [destination]

 * `-h` - Produce help text
 * `-c` - Copy rather than move the files
 * `-v` - Print progress report of processed files and directories
 * `-d` - Print debug messages
 * `source`... - One or more source files or folders
 * `destination` - A file name or folder where a single source is specified
                   or a folder name where there is more than one source

